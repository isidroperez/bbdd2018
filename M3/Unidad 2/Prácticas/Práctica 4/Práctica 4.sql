﻿-- Consulta 1. Averigua el DNI de todos los clientes.
  SELECT dni FROM cliente;

-- Consulta 2. Consulta todos los datos de todos los programas.
  SELECT * FROM programa;

-- Consulta 3. Obtén un listado con los nombres de todos los programas.
  SELECT DISTINCT nombre FROM programa;

-- Consulta 4. Genera una lista con todos los comercios.
  SELECT * FROM comercio;

/* Consulta 5. Genera una lista de las ciudades con establecimientos donde se venden programas, sin que
               aparezcan valores duplicados (utiliza DISTINCT). */
  SELECT DISTINCT ciudad FROM comercio;

-- Consulta 6. Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados.
  SELECT DISTINCT nombre FROM programa;

-- Consulta 7. Obtén el DNI más 4 de todos los clientes.
  SELECT dni+4 FROM cliente;

-- Consulta 8. Haz un listado con los códigos de los programas multiplicados por 7.
  SELECT codigo*7 FROM programa;

-- Consulta 9. ¿Cuáles son los programas cuyo código es inferior o igual a 10?
  SELECT * FROM programa
  WHERE codigo<=10;

-- Consulta 10. ¿Cuál es el programa cuyo código es 11?
  SELECT * FROM programa
  WHERE codigo = 11;

-- Consulta 11. ¿Qué fabricantes son de Estados Unidos?
  SELECT * FROM fabricante
  WHERE pais = 'Estados Unidos';

-- Consulta 12. ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN
  SELECT * FROM fabricante
  WHERE pais NOT IN ('España');

-- Consulta 13. Obtén un listado con los códigos de las distintas versiones de Windows.
  SELECT codigo FROM programa
  WHERE nombre = 'Windows';

-- Consulta 14. ¿En qué ciudades comercializa programas El Corte Inglés?
  SELECT ciudad FROM comercio
  WHERE nombre = 'El Corte Inglés';

-- Consulta 15. ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN.
  SELECT * FROM comercio
  WHERE nombre NOT IN ('El Corte Inglés');

-- Consulta 16. Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN.
  SELECT codigo FROM programa
  WHERE nombre IN ('Windows', 'Access');

/* Consulta 17. Obtén un listado que incluya los nombres de los clientes de edades comprendidas esntre 10 y 25 y de los mayores
  de 50 años.*/
  SELECT nombre FROM cliente
  WHERE edad BETWEEN 10 AND 25 OR edad >50;

-- Consulta 18. Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados.
  SELECT DISTINCT nombre FROM comercio
  WHERE ciudad = 'Sevilla' OR ciudad = 'Madrid';

-- Consulta 19. ¿Qué clientes terminan su nombre en la letra "o"?
  SELECT nombre FROM cliente
  WHERE nombre LIKE '%o';

-- Consulta 20. ¿Qué clientes terminan su nombre en la letra "o" y, además, son mayores de 30 años?
  SELECT nombre FROM cliente
  WHERE nombre like '%o' AND edad >30;

/* Consulta 21. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre
                empiece por una A o una W. */
  SELECT * FROM programa
  WHERE version LIKE '%i' OR nombre LIKE 'A%' OR nombre LIKE 'W%';

/* Consulta 22. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre
                comience por una A y termine por una S. */
  SELECT * FROM programa
  WHERE version LIKE '%i' OR (nombre LIKE 'A%' AND nombre LIKE '%s');

/* Consulta 23. Obtén un listado en el que aparezcan los programas cuya versión fnalice por una letra i, y cuyo nombre no
                comience por una A. */
  SELECT * FROM programa
  WHERE version LIKE '%i' AND nombre NOT LIKE 'A%';

-- Consulta 24. Obtén una lista de empresas por orden alfabético ascendente.
  SELECT * FROM fabricante
  ORDER BY nombre;

-- Consulta 25. Genera un listado de empresas por orden alfabético descendente.
  SELECT * FROM fabricante
  ORDER BY nombre DESC;

-- Consulta 26. Obtén un listado de programas por orden de versión.
  SELECT * FROM programa
  ORDER BY version;

-- Consulta 27. Genera un listado de los programas que desarrolla Oracle.
  SELECT programa.* FROM programa JOIN desarrolla USING(codigo) JOIN fabricante USING(id_fab)
  WHERE fabricante.nombre = 'Oracle';

-- Consulta 28. ¿Qué comercios distribuye Windows?
  SELECT comercio.* FROM programa JOIN distribuye USING (codigo) JOIN comercio USING (cif)
  WHERE programa.nombre = 'Windows';

-- Consulta 29. Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid.
  SELECT programa.codigo, programa.nombre, COUNT(*) FROM programa JOIN distribuye USING(codigo) JOIN comercio USING(cif)
  WHERE comercio.nombre = 'El Corte Inglés' AND ciudad = 'Madrid'
  GROUP BY programa.codigo;

-- Consulta 30. ¿Qué fabricante ha desarrollado Freddy Hardest?
  SELECT fabricante.* FROM programa JOIN desarrolla USING (codigo) JOIN fabricante USING (id_fab)
  WHERE programa.nombre = 'Freddy Hardest';
 
-- Consulta 31. Selecciona el nombre de los programas que se registran por Internet.
  SELECT programa.* FROM programa JOIN registra USING (codigo)
  WHERE medio LIKE 'Internet';

-- Consulta 32. Selecciona el nombre de las personas que se registran por internet.
  SELECT nombre FROM cliente JOIN registra USING(dni)
  WHERE medio LIKE 'Internet';

-- Consulta 33. ¿Qué medios ha utilizado para registrarse Pepe Pérez?
  SELECT medio FROM registra JOIN cliente USING (dni)
  WHERE nombre = 'Pepe Pérez';

-- Consulta 34. ¿Qué usuarios han optado por Internet como medio de registro?
  SELECT nombre FROM cliente JOIN registra USING(dni)
  WHERE medio LIKE 'Internet';

-- Consulta 35. ¿Qué programas han recibido registros por tarjeta postal?
  SELECT nombre FROM programa JOIN registra USING(codigo)
  WHERE medio = 'tarjeta postal';

-- Consulta 36. ¿En qué localidades se han vendido productos que se han registrado por internet?
  SELECT ciudad FROM programa JOIN registra USING(codigo) JOIN distribuye USING(codigo) JOIN comercio ON distribuye.cif = comercio.cif
  WHERE medio LIKE 'Internet';

/* Consulta 37. Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de
                los programas para los que ha efectuado el registro. */
  SELECT cliente.nombre, programa.nombre FROM cliente JOIN registra USING(dni) JOIN programa USING(codigo);

/* Consulta 38. Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con lo que lo
                ha hecho y el comercio en el que lo ha adquirido. */
SELECT cliente.nombre, programa.nombre, medio, comercio.nombre FROM cliente JOIN registra USING(dni)
JOIN programa USING(codigo) JOIN comercio USING(cif);

-- Consulta 39. Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle.
SELECT DISTINCT ciudad FROM(
SELECT id_fab FROM fabricante WHERE nombre='Oracle') c1
JOIN desarrolla USING(id_fab)
JOIN distribuye USING (codigo)
JOIN comercio USING(cif);

-- Consulta 40. Obtén el nombre de los usuarios que han registrado Access XP.
  SELECT DISTINCT nombre FROM(SELECT codigo FROM programa WHERE nombre='Access' AND VERSION='xp') c1
  JOIN registra USING(codigo)
  JOIN cliente USING (dni);

-- Consulta 41. Nombre de aquellos fabricantes cuyo país es el mismo que 'Oracle'.
SELECT nombre FROM (SELECT id_fab, pais FROM fabricante WHERE nombre='Oracle')c1 JOIN fabricante ON c1.pais=fabricante.pais
AND c1.id_fab<>fabricante.id_fab;

-- Consulta 42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez.
  SELECT nombre FROM (SELECT dni, edad FROM cliente WHERE nombre='Pepe Pérez')c1 JOIN cliente ON c1.edad=cliente.edad
  AND c1.dni<>cliente.dni;

-- Consulta 43. Genera un listado con los comercios que tienen su sede en la misma ciudad que 'FNAC'.
SELECT distinct nombre FROM (SELECT cif, ciudad FROM comercio
WHERE nombre='FNAC') c1 JOIN comercio ON c1.ciudad=comercio.ciudad
AND c1.cif<>comercio.cif;

-- Consulta 44. Nombre de aquellos clientes que han registrado un producto  de la misma forma que el cliente 'Pepe Pérez'.
  SELECT DISTINCT nombre FROM(
    SELECT dni FROM (
    SELECT DISTINCT medio FROM(
    SELECT dni FROM cliente WHERE nombre='Pepe Pérez') c1
    JOIN registra USING(dni)) c2
    JOIN registra USING(medio)
    WHERE dni<>(SELECT dni FROM cliente WHERE nombre='Pepe Pérez')) c3
    JOIN cliente USING(dni);

 -- Consulta 45. Obtener el número de programas que hay en la tabla programas.
SELECT COUNT(DISTINCT nombre) FROM programa;

-- Consulta 46. Calcula el número de clientes cuya edad es mayor de 40 años.
SELECT COUNT(*) FROM cliente
WHERE edad >40;

-- Consulta 47. Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1.
SELECT SUM(cantidad) FROM distribuye
WHERE cif = 1;

-- Consulta 48. Calcula la media de programas que se venden cuyo código es 7.
SELECT AVG(cantidad) FROM distribuye
WHERE codigo=7;

-- Consulta 49. Calcula la mínima cantidad de programas de código 7 que se ha vendido.
SELECT min(cantidad) FROM distribuye
WHERE codigo=7;

-- Consulta 50. Calcula la máxima cantidad de programas de código 7 que se ha vendido.
SELECT max(cantidad) FROM distribuye
WHERE codigo=7;

-- Consulta 51. ¿En cuántos estableciemientos se vende el programa cuyo código es 7?
SELECT count(cantidad) FROM distribuye
WHERE codigo=7;

-- Consulta 52. Calcular el número de registros que se han realizado por Internet.
SELECT COUNT(*) FROM registra
WHERE medio = 'Internet';

-- Consulta 53. Obtener el número total de programas que se han vendido en 'Sevilla'.
SELECT SUM(cantidad) FROM distribuye JOIN comercio USING (cif)
WHERE ciudad = 'Sevilla';

-- Consulta 54. Calcular el número total de programas que han desarrollado los fabricantes cuyo país es 'Estados Unidos'.
SELECT COUNT(*) FROM desarrolla JOIN fabricante USING(id_fab)
WHERE pais = 'Estados Unidos';

/* Consulta 55. Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también
                la longitud de la cadena del nombre */
SELECT UPPER(nombre), CHAR_LENGTH(nombre) FROM cliente;

-- Consulta 56. Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA.
SELECT CONCAT(nombre, version) FROM programa;