﻿
-- 1. Listar todos los lenguajes oficiales de España (España es el LocalName).
SELECT Language FROM country JOIN countrylanguage ON Code=CountryCode
WHERE LocalName = 'España';

-- 2. Listar todos los lengaujes de España.
SELECT Language FROM country JOIN countrylanguage ON Code=CountryCode
WHERE LocalName = 'España' AND IsOfficial='T';

-- 3. Indicar la población media por continente y región (ordenándolo por ambos campos).
SELECT continent, region, AVG(Population) FROM country
GROUP BY Continent, Region;

-- 4. Indicar el nombre de todas las ciudades del continente cuya población media es la mayor.
  -- Vista 1. Media de todos los continentes.
CREATE OR REPLACE VIEW p4c1 AS
SELECT continent, AVG(country.Population) n FROM country
GROUP BY continent;
  -- Vista 2. Maximo de esas medias.
CREATE OR REPLACE VIEW p4c2 AS
SELECT MAX(n) maximo FROM p4c1;
  -- Vista 3. Union de ambas vistas con country para sacar código.
CREATE OR REPLACE VIEW p4c3 AS
SELECT code, maximo, country.continent FROM p4c2 JOIN p4c1 ON p4c1.n = p4c2.maximo JOIN country ON p4c1.continent=country.continent;
  -- Consulta definitiva.
SELECT name FROM p4c3 JOIN city ON CountryCode=p4c3.code;

-- 5. Indicar la población media de las ciudades del continente cuyos países tengan la esperanza de vida mayor.
  -- Vista 1. Esperanza de vida por continente - país.
CREATE OR REPLACE VIEW p5c1 AS
SELECT continent, AVG(LifeExpectancy) n FROM country
  GROUP BY continent;
  -- Vista 2. Máximo de esa edad
CREATE OR REPLACE VIEW p5c2 as
SELECT MAX(n) maximo FROM p5c1;

  -- Vista 3.Union de ambas con country para sacar código.
CREATE OR rEPLACE VIEW p5c3 as
SELECT code, maximo, country.continent FROM p5c2 JOIN p5c1 ON p5c1.n=p5c2.maximo JOIN country ON p5c1.continent=country.continent;

  -- Consulta definitiva.
SELECT name FROM p5c3 JOIN city ON CountryCode=code;

-- 6.Indicar la población y el nombre de las capitales de cada uno de los países. Además, debemos indicar el país del cual es la capital y el nombre del continente al que pertenece.
SELECT city.name, city.Population, country.name, Continent FROM country JOIN city ON capital=ID
GROUP BY Capital, country.name, continent;

-- 7. Indicar el lenguaje en cada país cuyo porcentaje sea el mayor.
  -- vista 1. porcentaje de cada país.
  CREATE OR REPLACE VIEW p7c1 as
    SELECT CountryCode,MAX(Percentage) m FROM countrylanguage GROUP BY CountryCode;

  -- vista 2. Lenguaje, país y máximo de porcentaje.
    CREATE OR REPLACE VIEW p7c2 as
    SELECT Language,c.CountryCode FROM countrylanguage c JOIN p7c1 p ON c.CountryCode=p.CountryCode AND Percentage=m;

  -- Consulta definitiva. Unir para sacar nombre de país.
    SELECT Name, language FROM p7c2 JOIN country ON Code=countrycode;