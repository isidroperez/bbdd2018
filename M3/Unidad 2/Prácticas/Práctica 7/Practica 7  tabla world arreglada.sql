﻿CREATE TABLE world.city (
  ID int(11) NOT NULL AUTO_INCREMENT,
  Name char(35) NOT NULL DEFAULT '',
  CountryCode char(3) NOT NULL,
  District char(20) NOT NULL DEFAULT '',
  Population int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (ID, CountryCode)
)
ENGINE = INNODB,
AUTO_INCREMENT = 4080,
AVG_ROW_LENGTH = 4,
CHARACTER SET utf8,
COLLATE utf8_spanish_ci;

ALTER TABLE world.city
ADD CONSTRAINT FKCityCountryCode FOREIGN KEY (CountryCode)
REFERENCES world.country (Code);