﻿-- Consulta 1. Visualizar el número de empleados de cada departamento. Utilizar GROUP BY para agrupar por departamentoSELECT dept_no, COUNT(*) FROM emple
SELECT COUNT(*) n, dept_no FROM emple
GROUP BY dept_no;

-- Consulta 2. Visualizar los departamentos con más de 5 empleados. Utilizar GROUP BY para agrupar por departamento y HAVING para establecer la condición sobre los grupos.


SELECT dept_no, n FROM(SELECT COUNT(*) n, dept_no FROM emple
GROUP BY dept_no) c1
HAVING n >=5;

-- Consulta 3. Hallar la media de los salarios de cada departamento (utilizar la función avg y GROUP BY)
SELECT dept_no,avg(salario) FROM emple
GROUP BY dept_no;

-- Consulta 4. Visualizar el nombre de los empleados vendedores del departamento 'VENTAS' (Nombre del departamento='VENTAS', oficio='VENDEDOR'
SELECT apellido FROM emple JOIN depart USING(dept_no)
WHERE dnombre='VENTAS' AND oficio='VENDEDOR';

-- Consulta 5. Visualizar el número de vendedores del departamento 'VENTAS'(utilizar la función COUNT sobre la consulta anterior).
SELECT COUNT(*) FROM (SELECT apellido FROM emple JOIN depart USING(dept_no)
WHERE dnombre='VENTAS' AND oficio='VENDEDOR') c1;

-- Consulta 6. Visualizar los oficios de los empleados del departamento 'VENTAS'.
SELECT DISTINCT oficio FROM emple JOIN depart USING(dept_no)
WHERE dnombre='VENTAS';

-- Consulta 7. A partir de la tabla EMPLE, visualizar el número de empleados de cada departamento cuyo oficio sea 'EMPLEADO' (utilizar GROUP BY para agrupar por departamento. En la cláusula WHERE habrá que indicar que el oficio es 'EMPLEADO').
SELECT dept_no, COUNT(*) FROM emple
 WHERE oficio='EMPLEADO'
 GROUP BY dept_no;

-- Consulta 8. Visualizar el departamento con más empleados.
SELECT dept_no, count(*) n FROM emple
GROUP BY dept_no;

SELECT MAX(n) FROM (SELECT dept_no, count(*) n FROM emple
GROUP BY dept_no) c1;

SELECT dept_no, count(*) n FROM emple
GROUP BY dept_no HAVING n=(SELECT MAX(n) FROM (SELECT dept_no, count(*) n FROM emple
GROUP BY dept_no) c1);

-- Consulta 9. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados.
SELECT dept_no, SUM(salario) FROM emple
GROUP BY dept_no;

SELECT AVG(salario) FROM emple;

SELECT dept_no, SUM(salario) n1 FROM emple
GROUP BY dept_no
HAVING n1>(SELECT AVG(salario) FROM emple);

-- Consulta 10. Para cada oficio obtener la suma de salarios.
SELECT oficio, SUM(salario) n FROM emple
GROUP BY oficio;

-- Consulta 11. Visualizar la suma de salarios de cada oficio del departamento 'VENTAS'.
SELECT oficio, SUM(salario) n FROM emple JOIN depart USING(dept_no)
WHERE dnombre = 'VENTAS';

-- Consulta 12. Visualizar el número de departamento que tenga más empleados cuyo oficio sea empleado.
-- Creando c1 con vistas
CREATE OR REPLACE VIEW c1 AS 
  SELECT COUNT(*) n, dept_no FROM emple
  WHERE oficio = 'Empleado' 
  GROUP BY dept_no;
-- Creando c2 con vistas
CREATE OR REPLACE VIEW c2 AS 
  SELECT MAX(n) maximo  FROM c1;
 
-- Creando c2 sin vistas 
  SELECT MAX(n) maximo FROM (SELECT COUNT(*) n, dept_no FROM emple
  WHERE oficio = 'Empleado'
  GROUP BY dept_no) c1;

-- Forma más fácil con vistas
SELECT dept_no FROM c1 WHERE n=(SELECT * FROM c2);
-- Forma join con vistas
SELECT dept_no FROM c1 JOIN c2 ON maximo=n;

-- Tocho
SELECT dept_no FROM emple
WHERE oficio = 'Empleado'
GROUP BY dept_no
HAVING COUNT(*)=(SELECT MAX(n) FROM (SELECT COUNT(*) n, dept_no FROM emple
WHERE oficio = 'Empleado'
GROUP BY dept_no) c1);

-- Tocho mas grande con join
SELECT dept_no FROM 
(SELECT COUNT(*) n, dept_no FROM emple
WHERE oficio = 'Empleado'
GROUP BY dept_no) c1 
  JOIN
  (SELECT MAX(n) maximo FROM (SELECT COUNT(*) n, dept_no FROM emple
WHERE oficio = 'Empleado'
GROUP BY dept_no) c1) c2
ON maximo=n;

-- Consulta 13. Mostrar el número de oficios distintos de cada departamento.
SELECT dept_no, COUNT(DISTINCT oficio) FROM emple
GROUP BY dept_no;

-- Consulta 14. Mostrar los departamentos que tengan más de dos personas trabajando en la misma profesión.
SELECT DISTINCT dept_no FROM emple GROUP BY dept_no,oficio HAVING COUNT(*)>=2;

-- Consulta 15. Dada la tabla HERRAMIENTAS, visualizar por cada estantería la suma de las unidades.
SELECT estanteria, SUM(unidades) FROM herramientas
GROUP BY estanteria;

-- Consulta 16. Visualizar la estantería con más unidades de la tabla HERRAMIENTAS.
CREATE OR REPLACE VIEW c1cons16 AS 
  SELECT estanteria, SUM(unidades) n FROM herramientas
GROUP BY estanteria;

CREATE OR REPLACE VIEW c1cons17 as
SELECT MAX(n) maximo FROM c1cons16;

SELECT estanteria FROM c1cons16 JOIN c1cons17 ON n=maximo;

-- Consulta 17. Mostrar el número de médicos que pertenecen a cada hospital, ordenado por número descendente de hospital.
SELECT COUNT(*) n, cod_hospital FROM medicos
GROUP BY cod_hospital
 ORDER BY n DESC;

-- Consulta 18. Realizar una consulta en la que se muestre por cada hospital el nombre de las especialidades que tiene.
SELECT especialidad, nombre FROM hospitales JOIN medicos USING(cod_hospital)
GROUP BY nombre;

-- Consulta 19. Realizar una consulta en la que aparezca por cada hospital y en cada especialidad el número de médicos (tendrás que partir de la consulta anterior y utilizar GROUP BY)
CREATE OR REPLACE VIEW c1cons19 AS
  SELECT especialidad, nombre, hospitales.cod_hospital FROM hospitales JOIN medicos USING(cod_hospital)
GROUP BY nombre;

SELECT medicos.especialidad, nombre, COUNT(*) n FROM c1cons19 JOIN medicos USING(cod_hospital)
GROUP BY nombre, especialidad;

-- Consulta 20. Obtener por cada hospital el número de empleados.
SELECT cod_hospital, COUNT(*) FROM medicos
GROUP BY cod_hospital;

-- Consulta 21. Obtener por cada especialidad el número de trabajadores.
SELECT COUNT(*), especialidad FROM medicos
GROUP BY especialidad;

-- Consulta 22. Visualizar la especialidad que tenga más médicos.
CREATE OR REPLACE VIEW c1cons22 AS
SELECT COUNT(*) n, especialidad FROM medicos
GROUP BY especialidad;

CREATE OR REPLACE VIEW c2cons22 AS
SELECT MAX(n) maximo FROM c1cons22;

SELECT especialidad, n FROM c1cons22 JOIN c2cons22 ON n=maximo;

-- Consulta 23. Cuál es el nombre del hospital que tiene mayor número de plazas.
CREATE OR REPLACE VIEW c1cons23 as
SELECT MAX(num_plazas) n FROM hospitales;

SELECT nombre, n FROM c1cons23 JOIN hospitales ON n=num_plazas;

-- Consulta 24. visualizar las diferentes estanterías de la tabla HERRAMIENTAS ordenados descendentemente por estantería.
SELECT DISTINCT estanteria FROM herramientas
ORDER BY estanteria DESC;

-- Consulta 25. Averiguar cuántas unidades tiene cada estantería.
SELECT estanteria, SUM(unidades) n FROM herramientas
GROUP BY estanteria;

-- Consulta 26. Visualizar las estanterías que tengan más de 15 unidades.
SELECT estanteria, SUM(unidades) n FROM herramientas
  GROUP BY estanteria
HAVING n>15;

-- Consulta 27. ¿Cuál es la estantería que tiene más unidades?
CREATE OR REPLACE VIEW c1cons27 AS
SELECT estanteria, sum(unidades) suma FROM herramientas
GROUP BY estanteria;

SELECT estanteria, MAX(suma) maximo FROM c1cons27;

-- Consulta 28. A partir de las tablas EMPLE y DEPART mostrar los datos del departamento que no tiene ningún empleado.
SELECT depart.* FROM depart LEFT JOIN emple USING(dept_no)
WHERE emp_no IS NULL;

-- Consulta 29. Mostrar el número de empleados de cada edepartamento. En la salida se debe mostrar también los departamentos que no tienen ningún empleado.
SELECT dept_no, COUNT(*) FROM emple JOIN depart USING(dept_no)
GROUP BY depart.dept_no
UNION
SELECT dept_no, COUNT(emp_no) FROM depart LEFT JOIN emple USING(dept_no)
WHERE emp_no IS NULL;

-- Consulta 30. Obtener la suma de salarios de cada departamento, mostrando las columnas DEPT_NO, SUMA DE SALARIOS Y DNOMBRE. En el resultado también se deben mostrar los departamentos que no tienen asignados empleados.
SELECT depart.dept_no, SUM(salario) SUMA_DE_SALARIOS, dnombre FROM depart JOIN emple USING(dept_no)
GROUP BY depart.dept_no
UNION
SELECT dept_no, SUM(salario) SUMA_DE_SALARIOS, dnombre FROM depart LEFT JOIN emple USING(dept_no)
WHERE emp_no IS NULL;

-- Consulta 31. Utilizar la función IFNULL en la consutla anterior para que en el caso de que un departamento no tenga empleados, aparezca como suma de salarios el valor 0.
SELECT dept_no, IFNULL(SUMA_DE_SALARIOS, 0), dnombre FROM (SELECT depart.dept_no, SUM(salario) SUMA_DE_SALARIOS, dnombre FROM depart JOIN emple USING(dept_no)
GROUP BY depart.dept_no
UNION
SELECT dept_no, SUM(salario) SUMA_DE_SALARIOS, dnombre FROM depart LEFT JOIN emple USING(dept_no)
WHERE emp_no IS NULL) c1;

-- Consulta 32. Obtener el número de médicos que pertenece a cada hospital, mostrando las columnas COD_HOSPITAL, NOMBRE Y NÚMERO DE MÉDICOS. En el resultado deben aparecer también los datos de los hospitales que no tienen médicos.
SELECT hospitales.cod_hospital COD_HOSPITAL, nombre, COUNT(dni) numeromedicos FROM hospitales JOIN medicos USING(cod_hospital)
GROUP BY hospitales.cod_hospital
UNION
SELECT hospitales.cod_hospital COD_HOSPITAL, nombre, COUNT(dni) numeromedicos FROM hospitales LEFT JOIN medicos USING(cod_hospital)
WHERE dni IS NULL;
