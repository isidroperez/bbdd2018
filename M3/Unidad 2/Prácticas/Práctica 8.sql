﻿-- Consulta 4.1. Número de habitaciones por cada hotel (solo indicar el número del hotel).
SELECT NUM_HOTEL, COUNT(*) n FROM tipo_habitacion
  GROUP BY NUM_HOTEL;

-- Consulta 4.2. Número de habitaciones por cada hotel (indicar el nombre y la dirección del hotel).
SELECT hotel.NOMBRE, DOMICILIO, COUNT(*) n FROM tipo_habitacion JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.NOMBRE;

-- Consulta 4.3. Indicar le nombre del hotel con mayor número de habitaciones.

-- Vista 1
CREATE OR REPLACE VIEW p4c3c1 AS
  SELECT NUM_HOTEL, COUNT(*) n FROM tipo_habitacion
  GROUP BY NUM_HOTEL;

-- Vista 2.
CREATE OR REPLACE VIEW p4c3c2 AS
  SELECT MAX(n) FROM p4c3c1;

-- Consulta.
SELECT hotel.NOMBRE, DOMICILIO, COUNT(*) maximo FROM tipo_habitacion JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.NOMBRE
  HAVING maximo = (SELECT MAX(n) FROM p4c3c1);

-- Consulta 4.4. Indicar el nombre del hotel(o hoteles) con el segundo mayor número de habitaciones.
SELECT DISTINCT COUNT(*) maximo FROM tipo_habitacion JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.NOMBRE
  ORDER BY MAXIMO DESC
  LIMIT 1,1;

SELECT hotel.NOMBRE, DOMICILIO, COUNT(*) maximo FROM tipo_habitacion JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.NOMBRE
  HAVING maximo = (SELECT DISTINCT COUNT(*) maximo FROM tipo_habitacion JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.NOMBRE
  ORDER BY MAXIMO DESC
  LIMIT 1,1);

-- Consulta 4.5. Indicar el nombre de los hoteles que todavia no tenemos habitaciones introducidas.
SELECT DISTINCT hotel.NOMBRE FROM hotel LEFT JOIN tipo_habitacion USING(NUM_HOTEL)
  WHERE tipo_habitacion.NUM_HOTEL IS NULL;


-- Consulta 4.6. indicar el nombre del hotel del cual no se han reservado nunca habitaciones (teniendo en cuenta solamente los hoteles de los cuales hemos introducido sus habitaciones).
-- Hoteles en los que si que se han reservado.
CREATE OR REPLACE VIEW c1 AS 
SELECT DISTINCT NUM_HOTEL FROM reserva JOIN tipo_habitacion ON reserva.NUM_TIPOHAB = tipo_habitacion.NUM_TIPOHAB;
-- Hoteles donde no se ha reservado.
CREATE OR REPLACE VIEW c2 AS 
SELECT DISTINCT NUM_HOTEL FROM  tipo_habitacion LEFT JOIN reserva ON tipo_habitacion.NUM_TIPOHAB = reserva.NUM_TIPOHAB WHERE reserva.NUM_TIPOHAB IS NULL;
-- Hoteles donde nunca se ha reservado.

CREATE OR REPLACE VIEW c3 AS 
SELECT c2.num_hotel FROM c2 LEFT JOIN c1 USING(num_hotel) WHERE c1.num_hotel IS NULL;

SELECT * FROM c3 JOIN hotel USING(NUM_HOTEL);

DROP VIEW c1,c2,c3;

-- Consulta 7. Indicar el mes que mas reservas han comenzado.
 -- Reservas por mes
  CREATE OR REPLACE VIEW c7c1 as
 SELECT MONTH(FECHA_INI) fecha, COUNT(NUM_RESERVA) n FROM reserva
    GROUP by fecha;
 -- Maximo.
  CREATE OR REPLACE VIEW c7c2 AS
 SELECT MAX(n) FROM c7c1;
 -- Consulta.
  SELECT MONTH(FECHA_INI) fecha, COUNT(NUM_RESERVA) maximo FROM reserva
    GROUP BY fecha
    HAVING maximo=(SELECT * FROM c7c2);

-- Consulta 8. Listar el num_hotel y los distintos meses en que se han comenzado reservas.
 SELECT NUM_HOTEL, MONTH(FECHA_INI) fecha FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB)
    GROUP by NUM_HOTEL, fecha;

-- Consulta 9. Listar los nombres de los hoteles que han comenzado reservas en todos los meses que han comenzado reservas el hotel numero 4.

SELECT DISTINCT COUNT(MONTH(FECHA_INI)) fecha4 FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB)
WHERE num_hotel=4;

SELECT num_hotel, COUNT(DISTINCT MONTH(fecha_ini)) n FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB)
WHERE MONTH(FECHA_INI) IN (SELECT DISTINCT MONTH(FECHA_INI) fecha4 FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB)
WHERE num_hotel=4) AND NUM_HOTEL NOT IN (4)
GROUP BY num_hotel
HAVING n = (SELECT DISTINCT COUNT(MONTH(FECHA_INI)) fecha4 FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB)
WHERE num_hotel=4);

-- Consulta 10. Listar los días totales que se han reservado cada una de las habitaciones. Indicar en el listado el nombre del hotel al que pertenece.
SELECT hotel.NOMBRE, NUM_TIPOHAB, SUM(DATEDIFF(FECHA_FIN, FECHA_INI)) FROM reserva JOIN tipo_habitacion USING(NUM_TIPOHAB) JOIN hotel USING(NUM_HOTEL)
  GROUP BY hotel.nombre, NUM_TIPOHAB;

-- Consulta 11. Indicar el nombre de los 2 hoteles que mayor número de habitaciones se han reservado durante el primer trimestre de cualquier año.
SELECT hotel.nombre, COUNT(NUM_RESERVA) cuenta, QUARTER(FECHA_INI) trimestre, year(FECHA_INI) año FROM reserva
  JOIN tipo_habitacion USING(NUM_TIPOHAB)
  JOIN hotel USING(NUM_HOTEL)
  GROUP BY trimestre, año
having trimestre=1
ORDER BY cuenta DESC LIMIT 2;