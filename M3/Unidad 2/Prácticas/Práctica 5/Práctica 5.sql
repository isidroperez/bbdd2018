﻿DROP DATABASE IF EXISTS Practica5;
CREATE DATABASE IF NOT EXISTS Practica5;
USE Practica5;
CREATE OR REPLACE TABLE FEDERACION(
  nombre varchar(15),
  direccion varchar(35),
  telefono varchar(9),
  PRIMARY KEY(nombre)
  );

CREATE OR REPLACE TABLE MIEMBRO(
  DNI char(9),
  nombre_m varchar(15),
  Titulacion varchar(15),
  PRIMARY KEY (DNI)
  );

CREATE OR REPLACE TABLE COMPOSICION(
  nombre varchar(15),
  DNI char(9),
  cargo varchar (15),
  fecha_inicio date,
  CONSTRAINT FKComposicionNombre FOREIGN KEY(nombre)
  REFERENCES FEDERACION(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKComposicionDNI FOREIGN KEY(DNI)
  REFERENCES MIEMBRO(DNI) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY(nombre, DNI)
  );

SELECT * FROM COMPOSICION;

-- Consulta 1.
  SELECT nombre_m FROM COMPOSICION JOIN MIEMBRO USING(DNI)
  WHERE cargo = 'Presidente';

-- Consulta 2.
  SELECT DISTINCT direccion FROM COMPOSICION JOIN FEDERACION USING(nombre)
  WHERE cargo='Gerente';

-- Consulta 3.
  SELECT nombre FROM FEDERACION LEFT JOIN (SELECT nombre FROM COMPOSICION WHERE cargo ='Asesor Tecnico') c1 USING(nombre)
  WHERE c1.nombre IS NULL;


-- Consulta 4.
  SELECT nombre, COUNT(DISTINCT cargo) FROM COMPOSICION
  GROUP BY nombre;
  
  SELECT COUNT(DISTINCT cargo) FROM COMPOSICION;

  SELECT nombre, COUNT(DISTINCT cargo) n FROM COMPOSICION
  GROUP BY nombre
  HAVING n= (SELECT COUNT(DISTINCT cargo) FROM COMPOSICION);

-- Consulta 5.
  SELECT c1.nombre FROM (SELECT nombre FROM composicion
  WHERE cargo = 'Asesor Tecnico') c1 JOIN
  (SELECT nombre FROM composicion WHERE cargo='Psicologo') c2
  USING(nombre);
 