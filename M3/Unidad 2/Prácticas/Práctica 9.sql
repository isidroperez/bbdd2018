﻿-- Consulta 4a. Crear una consulta para cada género el máximo y el mínimo precio de las películas.
SELECT CodGenero, MAX(Precio), MIN(precio) FROM peliculas
  GROUP BY CodGenero;

-- Consulta 4b. Crear una consulta que muestre por cada génro la suma de los precios de las películas.
  SELECT CodGenero, SUM(precio) FROM peliculas
  GROUP BY CodGenero;

-- Consulta 4c. Crear una consulta que muestre para cada vendedor el número de películas diferentes que ha vendido.
  SELECT CodVend, COUNT(DISTINCT CodPelicula) FROM facturas JOIN lineas_fac ON CodFac=CdFac
    GROUP BY CodVend;

-- Consulta 4d. Crear una consulta que muestre por cada vendedor el número total de películas que ha vendido en 2005 y 2006.
  SELECT CodVend, COUNT(*) FROM facturas
  WHERE year(fecha) BETWEEN 2005 AND 2006
  GROUP BY CodVend;

-- Consulta 4e. Crear una consulta que muestre para cada vendedor y por cada género: el número total de películas que ha vendido, el importe total de la venta y el importe medio vendido.
SELECT CodVend, CodGenero, COUNT(CodFac),SUM(importe+GastoEnvio+IVA-DTO), AVG(importe+GastoEnvio+IVA-DTO) FROM lineas_fac JOIN facturas ON CodFac=CdFac JOIN peliculas USING (CodPelicula)
GROUP BY codvend, CodGenero;

-- Consulta 4f. Crear una consulta que muestre para cada vendedor el número total de películas que ha vendido, el importe total de la venta y el importe medio vendido, en cada uno de los meses de 2006.
SELECT CodVend, COUNT(*)cuenta, SUM(importe+GastoEnvio+IVA-DTO) suma, AVG(importe+GastoEnvio+IVA-DTO) media, MONTH(Fecha) mes FROM facturas
 WHERE year(fecha)=2006
 GROUP BY codvend, fecha
 ORDER BY Fecha, codvend;

-- Consulta 4g. Crear una consulta que muestre para vada vendedor el número total de películas que ha vendido, el importe total de la venta y el importe medio vendido, en cada uno de los trimestres de 2005.
SELECT codvend, COUNT(*) cuenta, SUM(importe+GastoEnvio+IVA-DTO) suma, AVG(importe+GastoEnvio+IVA-DTO), QUARTER(fecha) trimestre FROM facturas
  WHERE year(fecha)=2005
  GROUP BY codvend, Fecha
  ORDER BY fecha, codvend;

-- Consulta 4h. Crear una consulta que calcules el número de facturas y el total de dichas facturas por cliente, para todos los clientes.
SELECT CodCli, COUNT(*) cuenta, SUM(importe+GastoEnvio+IVA-DTO) ImporteTotal FROM facturas
  GROUP BY CodCli;

-- Consulta 4i. Crear una consulta que calcule el número de películas y su importe total antes de impuestos y descuentos, por cliente, para todos los clientes.
SELECT codcli, COUNT(*), SUM(Importe) FROM facturas
  GROUP BY codcli;

-- Consulta 5a. Sumar a la comisión de los vendedores un 10%
  UPDATE vendedores
    set Comisión=Comisión*1.1;

-- Consutla 5b. Colocar en la tabla facturas un 10% mas en los gastos de envio las películas enviadas en 2004.
  SELECT * FROM facturas
    WHERE year(fecha)=2004;
  
  UPDATE facturas
    set GastoEnvio=GastoEnvio*1.1
    WHERE year(fecha)=2004;

-- Consulta 5c. Actualizar los precios de las películas en la tabla Lineas_Fac para que coincidan con los metidos en la tabla peliculas.
SELECT * FROM lineas_fac;
SELECT * FROM peliculas;

UPDATE lineas_fac JOIN (SELECT CodPelicula, precio n FROM peliculas) c1 USING(CodPelicula)
  SET Precio=c1.n;

-- Consulta 5.d. Colocar un IVA del 21 en todas las facturas de clientes que pertenezcan a una localidad de la provincia de Madrid.
SELECT NombLoc loc, NombPro prov, clientes.CodCli cli FROM localidades JOIN provincias USING(CodPro) JOIN clientes USING(CodLoc) JOIN facturas USING(CodCli)
  WHERE NombPro='Madrid';

UPDATE facturas JOIN (SELECT NombLoc loc, NombPro prov, clientes.CodCli cli FROM localidades JOIN provincias USING(CodPro) JOIN clientes USING(CodLoc) JOIN facturas USING(CodCli)
  WHERE NombPro='Madrid') c1 ON cli=CodCli
  SET IVA=21;

SELECT * FROM facturas;

-- Consulta 6a. Crear una tabla llamada localidades_VIP que contenga el CodCli y el NombLoc de todos los clientes que tengan mas de 5 facturas.
CREATE OR REPLACE VIEW c1 as
SELECT CodCli, NombLoc, COUNT(*) n FROM facturas JOIN clientes USING(CodCli) JOIN localidades USING(CodLoc)
  GROUP BY codcli;

SELECT * FROM c1
   where n=5;
CREATE OR REPLACE TABLE localidades_VIP
AS SELECT * FROM c1
   where n=5;

SELECT * FROM localidades_VIP;

-- Consulta 6b. Crear una tabla llamada PeliculasCompradas donde se coloquen los campos CodPelicula y CodCli. Tienen que salir los clientes con las peliculas compradas.
SELECT  clientes.CodCli, CodPelicula FROM facturas JOIN clientes USING(CodCli) JOIN lineas_fac ON CdFac=CodFac
GROUP BY codcli
ORDER BY clientes.CodCli;

CREATE TABLE PeliculasCompradas AS
  SELECT  clientes.CodCli, CodPelicula FROM facturas JOIN clientes USING(CodCli) JOIN lineas_fac ON CdFac=CodFac
GROUP BY codcli
ORDER BY clientes.CodCli;

-- Consulta 6c. Crear una tabla llamada VentasVendedores donde tengamos dos campos CodVen y cantidad que ha ganado (utilizando el campo importe de factura)
SELECT CodVend, SUM(importe) CantidadGanada FROM facturas 
GROUP BY CodVend;

CREATE TABLE VentasVendedores AS
  SELECT CodVend, SUM(importe) CantidadGanada FROM facturas 
GROUP BY CodVend;

-- Consulta 7a. Insertar en la tabla localidades_VIP los clientes que tengan 4 facturas.

CREATE OR REPLACE VIEW c7a as
SELECT CodCli, COUNT(*) n FROM facturas
  GROUP BY codcli;

SELECT codcli, NombLoc, n FROM c7a JOIN clientes USING(CodCli) JOIN localidades USING(CodLoc)
  HAVING n=4;

  INSERT INTO localidades_VIP
   SELECT codcli, NombLoc, n FROM c7a JOIN clientes USING(CodCli) JOIN localidades USING(CodLoc)
   HAVING n=4;

  SELECT * FROM localidades_VIP;

-- Consulta 7b. Crear una tabla (consulta de creación de tabla LDD) llamada Peliculas_Bajas con un solo campo denominado CodPelicula. Con una consulta de datos anexados meter las películas que no se han vendido.

  CREATE TABLE peliculas_bajas(
    codPelicula int,
    PRIMARY KEY(codPelicula)
    );

  INSERT INTO peliculas_bajas
    SELECT peliculas.CodPelicula FROM peliculas LEFT JOIN lineas_fac USING(CodPelicula) WHERE lineas_fac.CodPelicula IS NULL;

