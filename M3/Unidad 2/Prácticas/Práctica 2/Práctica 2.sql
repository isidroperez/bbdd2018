﻿
-- Consulta 1. Indicar el número de ciudades que hay en la tabla ciudades.
    SELECT COUNT(*) FROM ciudad;

-- Consulta 2. Indicar el nombre de las ciudades que tengan una población por encima de la población media.
  -- C1. Población media.
    SELECT AVG(población) FROM ciudad;
  -- Consulta.
    SELECT nombre FROM ciudad
    WHERE población>(SELECT AVG(población) FROM ciudad);
 
-- Consulta 3. Indicar el nombre de las ciuades que tengan una población por debajo de la población media.
 -- C1. Población media.
    SELECT AVG(población) FROM ciudad;
  -- Consulta.
    SELECT nombre FROM ciudad
    WHERE población<(SELECT AVG(población) FROM ciudad);

-- Consulta 4. Indicar el nombre de la ciudad con la población máxima.
  SELECT nombre FROM ciudad
  WHERE población=(SELECT MAX(población) FROM ciudad);

-- Consulta 5. Indicar el nombre de la ciudad con la población mínima.
  SELECT nombre FROM ciudad
  WHERE población=(SELECT MIN(población) FROM ciudad);

-- Consulta 6. Indicar el número de ciudades que tengan una población por encima de la población media.
  SELECT COUNT(*) FROM ciudad
  WHERE población>(SELECT AVG(población) FROM ciudad);

-- Consulta 7. Indicarme el número de personas que viven en cada ciudad.
  SELECT * FROM ciudad
  GROUP BY nombre;

-- Consulta 8. Utilizando la tabla trabaja indicarme cuantas personas trabajan en cada una de las compañías.
  SELECT compañia, COUNT(*) n FROM trabaja
  GROUP BY compañia;

-- Consulta 9. Indicarme la compañía que más trabajadores tiene.
  -- c1 máximo de trabajadores en una compañía. 
 SELECT MAX(n) FROM (SELECT compañia, COUNT(*) n FROM trabaja
  GROUP BY compañia) c1;
  -- Consulta.
  SELECT compañia, count(*) n FROM trabaja
  GROUP BY compañia
    HAVING n=(SELECT MAX(n) FROM (SELECT compañia, COUNT(*) n FROM trabaja
  GROUP BY compañia)c1);

-- Consulta 10. Indicarme el salario medio de cada una de las compañias.
  SELECT AVG(salario), compañia FROM trabaja
    GROUP BY compañia;

-- Consulta 11. Listarme el nombre de las personas y la población de la ciudad donde viven.
  SELECT persona.nombre, población FROM persona JOIN ciudad ON ciudad.nombre=ciudad;

-- Consulta 12. Listar el nombre de las personas, la calle donde vive y la población de la ciudad donde vive.
  SELECT persona.nombre, calle, población FROM persona JOIN ciudad ON ciudad.nombre=ciudad;

-- Consulta 13. Listar el nombre de las ,personas, la ciudad donde vive y la ciudad donde está la compañía para la que trabaja.
  SELECT persona.nombre, persona.ciudad CiudadPersona, compañia.ciudad CiudadCompañia FROM persona
    JOIN ciudad ON ciudad.nombre=ciudad
    JOIN trabaja ON persona=persona.nombre
    JOIN compañia ON compañia=compañia.nombre;

-- Consulta 14. Realizar algebra relacional y explicar la siguiente consulta.
  SELECT persona.nombre
  FROM persona, trabaja, compañia
  WHERE persona.nombre = trabaja.persona
  AND trabaja.compañia=compañia.nombre
  AND compañia.ciudad=persona.ciudad
  ORDER BY persona.nombre;
  
  -- Son personas que trabajan en la misma ciudad donde está la compañía, ordenadas por el nombre de las personas.

-- Consulta 15. Listarme el nombre de la persona y el nombre de su supervisor.
  SELECT nombre, supervisor FROM persona JOIN supervisa ON nombre=persona;

-- Consulta 16. Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos.
  SELECT persona, supervisor, p1.ciudad, p2.ciudad FROM supervisa
    JOIN persona p1 ON persona=p1.nombre
    JOIN persona p2 ON supervisor=p2.nombre
   ORDER BY persona;

-- Consulta 17. Indicarme el número de ciudades distintas que hay en la tabla compañía.
  SELECT COUNT(DISTINCT ciudad) FROM compañia;

-- Consulta 18. Indicarme el número de ciudades distintas que hay en la tabla personas.
  SELECT COUNT(DISTINCT ciudad) FROM persona;

-- Consulta 19. Indicarme el nombre de las personas que trabajan para fagor.
  SELECT persona FROM trabaja
    WHERE compañia='FAGOR';

-- Consulta 20. Indicarme el nombre de las personas que no trabajan para fagor.
  SELECT persona FROM trabaja
    WHERE compañia NOT LIKE 'FAGOR';

-- Consulta 21. Indicarme el número de personas que trabajan para INDRA.
  SELECT COUNT(*) FROM trabaja
    WHERE compañia = 'INDRA';

-- Consulta 22. Indicarme el nombre de las personas que trabajan para FAGOR o INDRA.
  SELECT persona FROM trabaja
    WHERE compañia = 'INDRA' OR compañia = 'FAGOR';

/* Consulta 23. Listar la población donde vive cada persona, sus salario, su nombre y la compañía para la que trabaja. Ordenar la salida por nombre de la persona
  y por salario de forma descendente */
  SELECT población, salario, persona.nombre, compañia
    FROM persona JOIN ciudad ON ciudad.nombre=ciudad JOIN trabaja ON persona.nombre=persona
    ORDER BY persona.nombre, salario DESC;
