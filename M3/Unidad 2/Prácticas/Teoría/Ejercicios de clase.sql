﻿CREATE OR REPLACE VIEW consulta1c1 AS
    SELECT DISTINCT e.numetapa, e.dorsal
    FROM etapa e
    JOIN puerto p ON e.numetapa = p.numetapa;

CREATE OR REPLACE VIEW consulta1C2 AS
   SELECT DISTINCT c.nomequipo
    FROM ciclista c
    JOIN consulta1c1 c1
    ON c.dorsal=c1.dorsal;

SELECT * FROM consulta1C2 c;

CREATE OR REPLACE VIEW consulta1 AS
  SELECT e.*
  FROM equipo e
  JOIN consulta1C2 c2 ON c2.nomequipo=e.nomequipo;
    

SELECT nombre, ciclista.dorsal FROM ciclista JOIN puerto ON ciclista.dorsal=puerto.dorsal
  WHERE altura>1200;

-- mejorarla a partir de subconsultas
-- C1
SELECT p.dorsal FROM puerto p
WHERE p.altura >1200;
-- Consulta completa
SELECT c.* FROM (SELECT p.dorsal FROM puerto p WHERE p.altura >1200) as c1
JOIN ciclista c USING(dorsal);

-- Utilizando vistas
-- Primera vista
CREATE OR REPLACE VIEW consulta2c1 AS
  SELECT p.dorsal FROM puerto p
  WHERE p.altura >1200;
-- Segunda vista
CREATE OR REPLACE VIEW Consulta2 AS
  SELECT c.* FROM consulta2c1 as c1
  JOIN ciclista c USING(dorsal);
