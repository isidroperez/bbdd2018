﻿-- C1 : Ciclistas que han ganado etapas
    SELECT DISTINCT e.dorsal FROM etapa e;

-- C2 : Ciclistas que han ganado puertos
    SELECT DISTINCT p.dorsal FROM puerto p;

-- C3 : Ciclistas que no han ganado puertos
    SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
      WHERE p.dorsal IS NULL;
   
   
-- Utilizando vistas
   CREATE OR REPLACE VIEW consulta4c1 AS
      SELECT DISTINCT e.dorsal FROM etapa e;

   CREATE OR REPLACE VIEW consulta4c2 AS
      SELECT DISTINCT p.dorsal FROM puerto p;

   CREATE OR REPLACE VIEW consulta4c3 AS
       SELECT c.dorsal FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal
       WHERE p.dorsal IS NULL;

  -- C1 - C3 : Ciclistas que han ganado etapas y que han ganado puertos
    SELECT * FROM consulta4c1 c1 LEFT JOIN consulta4c3 c3 USING(dorsal)
    WHERE c3.dorsal IS NULL;

    SELECT c1.dorsal FROM consulta4c1 c1
    WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c3);

  -- C1 - C2 : ciclistas que han ganado etapas y no han ganado puertos
    SELECT * FROM consulta4c1 c1 LEFT JOIN consulta4c2 c2 USING(dorsal)
    WHERE c2.dorsal IS NULL;
    
    SELECT c1.dorsal FROM consulta4c1 c1
    WHERE c1.dorsal NOT IN (SELECT * FROM consulta4c2);
    
  -- C1 UNION C2 : ciclistas que han ganado etapas mas los ciclistas que han ganado puertos
    SELECT * FROM consulta4c1
    UNION
    SELECT * FROM consulta4c2;

  -- C1 UNION C3 : ciclistas que han ganado etapas mas los ciclistas que no han ganado puertos
    SELECT * FROM consulta4c1
    UNION
    SELECT * FROM consulta4c3;

  -- C1 interseccion C3: ciclistas que han ganado etapas y ademas que no han ganado puertos
    SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c3 c3;
  
  -- C1 interseccion c2: ciclistas que han ganado etapas y ademas han ganado puertos 
    SELECT * FROM consulta4c1 c1 NATURAL JOIN consulta4c2 c2;


  -- C1
    SELECT DISTINCT dorsal FROM lleva;
  -- C2
    SELECT ciclista.dorsal FROM ciclista left JOIN lleva USING (dorsal)
    WHERE lleva.dorsal IS NULL;
  -- C3
    SELECT dorsal FROM etapa;
  -- C4
    SELECT dorsal FROM puerto;

-- VISTAS
  CREATE OR REPLACE VIEW Consulta5C1 AS
    SELECT DISTINCT dorsal FROM lleva;
  CREATE OR REPLACE VIEW Consulta5C2 AS
    SELECT ciclista.dorsal FROM ciclista left JOIN lleva USING (dorsal)
    WHERE lleva.dorsal IS NULL;
  CREATE OR REPLACE VIEW Consulta5C3 AS
    SELECT dorsal FROM etapa;
  CREATE OR REPLACE VIEW Consulta5C4 AS
    SELECT dorsal FROM puerto;

-- C3 - C1
SELECT * FROM Consulta5C3 c3 LEFT JOIN Consulta5C1 c1 USING(dorsal)
WHERE c1.dorsal IS NULL;
-- C3 union c2
SELECT * FROM Consulta5C3
UNION
SELECT * FROM Consulta5C2;
-- C3 - C2
SELECT * FROM consulta5c3 c3 LEFT JOIN consulta5c2 c2 USING(dorsal)
WHERE c2.dorsal IS NULL;
-- C3 union C1
SELECT * FROM Consulta5C3
UNION
SELECT * FROM consulta5c1;
-- C4 - C3 - C2
SELECT * FROM consulta5c4 c4
  LEFT JOIN consulta5c3 c3 USING(dorsal)
  LEFT JOIN consulta5c2 c2 USING(dorsal)
  WHERE c2.dorsal IS NULL AND c3.dorsal IS NULL;

-- C1 union C3
SELECT * FROM consulta5c1
UNION
SELECT * FROM consulta5c3;

-- C1 union C3 union C4
SELECT * FROM Consulta5C1
UNION
SELECT * FROM consulta5c3
UNION
SELECT * FROM consulta5c4;