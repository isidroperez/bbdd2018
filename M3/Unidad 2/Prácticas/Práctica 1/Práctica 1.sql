﻿-- Consulta 1
  SELECT * FROM emple e;

-- Consulta 2
  SELECT * FROM depart d;

-- Consulta 3
  SELECT e.apellido, e.oficio FROM emple e;

-- Consulta 4
  SELECT d.loc, d.dept_no FROM depart d;

-- Consulta 5
  SELECT * FROM depart d;

-- Consulta 6
  SELECT COUNT(*) FROM emple e;

-- Consulta 7
  SELECT * FROM emple e
  GROUP BY e.apellido;

-- Consulta 8
  SELECT * FROM emple e
  GROUP BY e.apellido DESC;

-- Consulta 9
  SELECT COUNT(*) FROM depart d;

-- Consulta 10
  SELECT (SELECT COUNT(*) FROM emple e)+(SELECT COUNT(*) FROM depart d);

-- Consulta 11
  SELECT * FROM emple e
  GROUP BY e.dept_no DESC;

-- Consulta 12
  SELECT * FROM emple e
  GROUP BY e.dept_no DESC, e.oficio ASC;

-- Consulta 13
  SELECT * FROM emple e
  GROUP BY e.dept_no DESC, e.apellido ASC;

-- Consulta 14
  SELECT e.emp_no FROM emple e
  WHERE e.salario >2000;

-- Consulta 15
  SELECT e.emp_no, e.apellido FROM emple e
  WHERE e.salario <2000;

-- Consulta 16
  SELECT * FROM emple e
  WHERE salario BETWEEN 1500 AND 2500;

-- Consulta 17
  SELECT * FROM emple e
  WHERE e.oficio = "ANALISTA";

-- Consulta 18
  SELECT * FROM emple e
  WHERE oficio = "ANALISTA" AND e.salario >2000;

-- Consulta 19
  SELECT e.apellido, e.oficio FROM emple e
  WHERE e.dept_no = 20;

-- Consulta 20
  SELECT COUNT(*) FROM emple e
  WHERE e.oficio = "vendedor";

-- Consulta 21
  SELECT * FROM emple e
  WHERE e.apellido LIKE "M%" OR "N%"
  GROUP BY e.apellido;

-- Consulta 22
  SELECT * FROM emple e
  WHERE e.oficio = "VENDEDOR"
  GROUP BY e.apellido;

-- Consulta 23
-- c1 
SELECT MAX(e.salario) FROM emple e;

-- final
  SELECT * 
    FROM 
      emple e 
    WHERE  
      e.salario=(SELECT MAX(e.salario) FROM emple e);

-- Consulta 24
SELECT emp_no, apellido FROM emple
  WHERE oficio = 'Analista' AND dept_no=10
  ORDER BY apellido, oficio;

-- Consulta 25
  SELECT distinct MONTH(fecha_alt) FROM emple
    GROUP BY fecha_alt;

-- Consulta 26
  SELECT DISTINCT year(fecha_alt) FROM emple
    GROUP BY fecha_alt;

-- Consulta 27
  SELECT DISTINCT DAY(fecha_alt) FROM emple
    GROUP BY fecha_alt;

-- Consulta 28
  SELECT apellido FROM emple
    WHERE salario > 2000 OR dept_no=20;

-- Consulta 29
  SELECT apellido, dnombre FROM emple JOIN depart ON depart.dept_no=emple.dept_no;

-- Consulta 30
  SELECT apellido, oficio, dnombre FROM emple JOIN depart ON depart.dept_no=emple.dept_no
    ORDER BY apellido;

-- Consulta 31
  SELECT dept_no, COUNT(*) NUMERO_DE_EMPLEADOS FROM emple
    GROUP BY dept_no;

-- Consulta 32
  SELECT dnombre, COUNT(*) FROM emple JOIN depart ON depart.dept_no=emple.dept_no
    GROUP BY dnombre;

-- Consulta 33
  SELECT apellido FROM emple
    ORDER BY oficio, apellido;

-- Consulta 34
  SELECT apellido FROM emple
    WHERE apellido LIKE 'A%';

-- Consulta 35
  SELECT apellido FROM emple
    WHERE apellido LIKE 'A%' OR apellido LIKE 'M%';

-- Consulta 36
  SELECT * FROM emple
    WHERE apellido NOT LIKE '%Z';

-- Consulta 37
  SELECT * FROM emple
  where apellido LIKE 'A%' AND oficio LIKE '%E%'
  ORDER BY oficio, salario DESC;