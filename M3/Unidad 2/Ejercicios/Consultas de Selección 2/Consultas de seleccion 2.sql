﻿-- Consultas de selección 2 - 1
SELECT COUNT(c.dorsal) FROM ciclista c;

-- Consultas de selección 2 - 2
SELECT COUNT(c.dorsal) FROM ciclista c
WHERE c.nomequipo = "Banesto";

-- Consultas de selección 2 - 3
SELECT AVG(edad) FROM ciclista c;

-- Consultas de selección 2 - 4
SELECT AVG(edad) FROM ciclista c
WHERE c.nomequipo = "Banesto";

-- Consultas de selección 2 - 5
SELECT AVG(edad) AS MediaEdad, c.nomequipo FROM ciclista c
GROUP BY c.nomequipo;

-- Consultas de selección 2 - 6
SELECT COUNT(c.dorsal), c.nomequipo FROM ciclista c
GROUP BY c.nomequipo;

-- Consultas de selección 2 - 7
SELECT COUNT(p.nompuerto) FROM puerto p;

-- Consultas de selección 2 - 8
SELECT COUNT(p.nompuerto) FROM puerto p
WHERE p.altura > 1500;

-- Consultas de selección 2 - 9
SELECT DISTINCT c.nomequipo FROM ciclista c
WHERE (SELECT COUNT(dorsal) FROM ciclista c) >4;

-- Consultas de selección 2 - 10
SELECT DISTINCT c.nomequipo FROM ciclista c
WHERE (SELECT COUNT(dorsal) FROM ciclista c) >4 AND c.edad BETWEEN 28 AND 32;

-- Consultas de selección 2 - 11
-- c1
SELECT COUNT(*) AS numero, e.dorsal FROM etapa e GROUP BY e.dorsal;

SELECT c.nombre,c1.numero FROM (SELECT COUNT(*) AS numero, e.dorsal FROM etapa e GROUP BY e.dorsal) AS c1 JOIN ciclista c ON c.dorsal=c1.dorsal;

-- Consultas de selección 2 - 12
-- c1
SELECT COUNT(*) AS numero, e.dorsal FROM etapa e GROUP BY e.dorsal;
  SELECT c1.dorsal, c1.numero FROM (SELECT COUNT(*) AS numero, e.dorsal FROM etapa e GROUP BY e.dorsal) AS c1 JOIN etapa e ON c1.dorsal=e.dorsal
    WHERE c1.numero>1;
