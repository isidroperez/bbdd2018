﻿-- Consulta 1 Nombre y edad de los ciclistas que han ganado etapas.
SELECT DISTINCT nombre, edad FROM ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal;

-- Consulta 2 Nombre y edad de los ciclistas que han ganado puertos.
SELECT DISTINCT nombre, edad FROM ciclista INNER JOIN puerto ON ciclista.dorsal=puerto.dorsal;

-- Consulta 3 Nombre y edad de los ciclistas que han ganado etapas y puertos.
SELECT DISTINCT nombre, edad FROM ciclista INNER JOIN etapa INNER JOIN puerto ON ciclista.dorsal=etapa.dorsal AND ciclista.dorsal=puerto.dorsal;

-- Consulta 4 Listar el director de los equipos que tengan ciclistas que hayan ganado alguna etapa.
SELECT DISTINCT director FROM ciclista INNER JOIN equipo INNER JOIN etapa ON ciclista.nomequipo=equipo.nomequipo AND ciclista.dorsal=etapa.dorsal;

-- Consulta 5 Dorsal y nombre de los ciclistas que hayan llevado algún maillot.
SELECT DISTINCT nombre, ciclista.dorsal FROM ciclista INNER JOIN lleva ON ciclista.dorsal=lleva.dorsal;

-- Consulta 6 Dorsal y nombre de los ciclistas que hayan llevado el maillot amarillo.
SELECT distinct ciclista.dorsal, nombre FROM ciclista INNER JOIN lleva INNER JOIN maillot ON ciclista.dorsal=lleva.dorsal AND lleva.código=maillot.código
WHERE color='Amarillo';

-- Consulta 7 Dorsal de los ciclistas que hayan llevado algún maillot y que han ganado etapas.
SELECT DISTINCT ciclista.dorsal FROM ciclista INNER JOIN lleva INNER JOIN etapa ON ciclista.dorsal=lleva.dorsal AND ciclista.dorsal=etapa.dorsal;

-- Consulta 8 Indicar el numetapa de las etapas que tengan puertos.
SELECT DISTINCT etapa.numetapa FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa;

-- Consulta 9 Indicar los km de las etapas que hayan ganado ciclistas del Banesto y que tengan puertos.
SELECT distinct kms FROM etapa INNER JOIN ciclista INNER JOIN puerto ON etapa.numetapa=puerto.numetapa AND ciclista.dorsal=etapa.dorsal
WHERE nomequipo = 'Banesto';

-- Consulta 10 Listar el número de ciclistas que hayan ganado alguna etapa con puerto.
SELECT COUNT(*) FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa;

-- Consulta 11 Indicar el nombre de los puertos que hayan sido ganados por ciclistas de Banesto.
SELECT DISTINCT nompuerto FROM puerto INNER JOIN ciclista ON ciclista.dorsal=puerto.dorsal
WHERE nomequipo = 'Banesto';

-- Consulta 12 Listar el número de etapas que tengan puerto que hayan sido ganados por ciclistas de Banesto con más de 200 km.
SELECT COUNT(*) FROM etapa INNER JOIN puerto INNER JOIN ciclista ON ciclista.dorsal=etapa.dorsal AND etapa.numetapa=puerto.numetapa
WHERE nomequipo = 'Banesto' AND kms > 200;


  -- Subconsulta a parte
  SELECT * FROM etapa INNER JOIN puerto ON etapa.dorsal=puerto.dorsal
  WHERE kms >200;
