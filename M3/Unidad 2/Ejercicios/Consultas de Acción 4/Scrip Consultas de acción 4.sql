﻿-- Ejercicio 4. Creando FOREIGN KEYS.
  -- FOREIGN KEYS de la tabla Alquileres.
ALTER TABLE alquileres ADD CONSTRAINT FKAlquilerCliente FOREIGN KEY(cliente)
REFERENCES clientes(codigo) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE alquileres ADD CONSTRAINT FKAlquilerBici FOREIGN KEY(bici)
REFERENCES bicis(bicis_id) ON DELETE CASCADE ON UPDATE CASCADE;

  -- FOREIGN KEYS de la tabla Averías.
ALTER TABLE averias ADD CONSTRAINT FKAveriaBicis FOREIGN KEY (bici)
REFERENCES bicis(bicis_id) on delete cascade on update CASCADE;               

-- Ejercicio 6. Realizar mediante consultas de actualización los siguientes cálculos en las tablas.
  -- a. Bicis.kms: sumar todos los km que se hayan realizado en los distintos alquileres en las bicis.

  UPDATE bicis LEFT JOIN (SELECT bici, SUM(kms) n FROM alquileres
    GROUP BY bici) c1 ON bici=bicis_id
  SET kms= IFNULL(n,0);
  
  -- b. Bicis.años: calcular los años que tiene la bici a fecha de hoy.

    UPDATE bicis JOIN (SELECT bicis_id, (year(NOW()) - year(fechaCompra)) n FROM bicis
      GROUP BY bicis_id) c1 ON c1.bicis_id=bicis.bicis_id
    SET años=n;

  -- c. Bicis.averias: numero de averias de esa bici.
  ALTER TABLE bicis ADD COLUMN averias float;

  UPDATE bicis left JOIN (SELECT bici, COUNT(*) n FROM averias GROUP BY bici) c1 ON bici=bicis_id
    SET averias=IFNULL(n,0);

  -- d. Bicis.Alquileres: el numero de veces que se ha alquilado esa bici.
  ALTER TABLE bicis ADD COLUMN alquileres float;

  UPDATE bicis LEFT JOIN (SELECT bici, COUNT(*) n FROM alquileres GROUP BY bici) c1 ON bici=bicis_id
    SET alquileres=IFNULL(n,0);

  -- e. Bicis.gastos: el gasto total sumando todas las averías.
  ALTER TABLE bicis ADD COLUMN gastos float;

  UPDATE bicis LEFT JOIN (SELECT bici, SUM(coste) n FROM averias GROUP BY bici) c1 ON bici=bicis_id
    SET gastos=IFNULL(n,0);

  -- f. Bicis.beneficios: suma de todas las ganancias de esa bici en los alquileres (sumando precios).
  ALTER TABLE bicis ADD COLUMN beneficios float;

  UPDATE bicis left JOIN (SELECT bici, SUM(precio_total) n FROM alquileres GROUP BY bici) c1 ON bici=bicis_id
    SET beneficios=IFNULL(n,0);

  -- g. Alquileres.Clientes: numero de alquileres de cada uno de los clientes
  ALTER TABLE alquileres ADD COLUMN clientes float;

  UPDATE alquileres JOIN (SELECT cliente, COUNT(*) n FROM alquileres GROUP BY cliente) c1 ON c1.cliente=alquileres.cliente
    SET clientes=IFNULL(n,0);
  
  -- h. Alquileres.descuento: colocar el descuento en porcentaje por cada cliente.
  UPDATE alquileres JOIN (SELECT codigo, descuento*100 n FROM clientes) c1 ON codigo=cliente
  SET descuento=IFNULL(n,0);

  -- i. Alquileres.precioTotal: calcular el precio a pagar por cada cliente en función del precio de la bici y del descuento.
CREATE OR REPLACE VIEW i1 AS
SELECT bicis_id, precio-(precio*descuento/100) preciototal FROM bicis JOIN alquileres ON bici=bicis_id;

UPDATE alquileres JOIN i1 ON i1.bicis_id=bici
  SET precio_total=preciototal;
