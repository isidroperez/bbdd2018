﻿-- Ejercicio 4
ALTER TABLE productos ADD COLUMN (DESCUENTO1 float);


SELECT producto, importe_base, importe_base*0.1 descuento FROM productos
WHERE rubro='Verduras';

UPDATE productos JOIN(
SELECT producto, importe_base, importe_base*0.1 descuento FROM productos
WHERE rubro='Verduras') c1
ON c1.producto=productos.producto
SET DESCUENTO1=descuento;

-- Ejercicio 5
ALTER TABLE productos ADD COLUMN (DESCUENTO2 float);

-- Atados
SELECT producto, importe_base, importe_base*0.2 FROM productos
WHERE `u/medida`='Atado';
-- No atados
SELECT producto, importe_base, importe_base*0.05 FROM productos
WHERE `u/medida` NOT LIKE 'Atado';

-- Atados
UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.2 descuento FROM productos
WHERE `u/medida`='Atado') c1
ON c1.producto=productos.producto
SET DESCUENTO2=descuento;

-- No atados
UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.05 descuento FROM productos
WHERE `u/medida` NOT LIKE 'Atado') c1
ON c1.producto=productos.producto
SET DESCUENTO2=descuento;

-- Ejercicio 6
ALTER TABLE productos ADD COLUMN (DESCUENTO3 float);

SELECT producto, importe_base, importe_base*0.2 FROM productos
WHERE rubro='Frutas' AND importe_base>15;

UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.2 descuento FROM productos
WHERE rubro='Frutas' AND importe_base>15) c1
USING(producto)
SET DESCUENTO3=descuento;

-- Ejercicio 7
ALTER TABLE productos ADD COLUMN (DESCUENTO4 float);

-- Granjas primavera y litoral
SELECT producto, importe_base, importe_base*0.5 descuento FROM productos
WHERE granja='primavera' or granja='litoral';
-- Resto
SELECT producto, importe_base, importe_base*0.25 descuento FROM productos
WHERE granja NOT LIKE 'primavera' and granja NOT LIKE 'litoral';

-- Granjas primavera y litoral
UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.5 descuento FROM productos
  WHERE granja='primavera' or granja='litoral') c1
  USING(producto)
  SET DESCUENTO4=descuento;

-- Resto
UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.25 descuento FROM productos
  WHERE granja NOT LIKE 'primavera' and granja NOT LIKE 'litoral') c1
  USING(producto)
  SET DESCUENTO4=descuento;


-- Ejercicio 8
ALTER TABLE productos ADD COLUMN (AUMENTO1 float);

SELECT producto, importe_base, importe_base*0.1 aumento FROM productos
WHERE (rubro='frutas' OR rubro ='verduras') AND (granja='la garota' OR granja='la pocha');

UPDATE productos JOIN(
SELECT producto, importe_base, importe_base*0.1 aumento FROM productos
WHERE (rubro='frutas' OR rubro ='verduras') AND (granja='la garota' OR granja='la pocha')) c1
USING (producto)
SET AUMENTO1=aumento;

-- Ejercicio 9
ALTER TABLE productos ADD COLUMN(PRESENTACIÓN char(1));

-- Atados
SELECT producto FROM productos
WHERE `u/medida`='Atado';

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE `u/medida`='Atado') c1
  USING (producto)
  SET PRESENTACIÓN=1;

-- Unidad
SELECT producto FROM productos
WHERE `u/medida`='unidad';

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE `u/medida`='unidad') c1
  USING (producto)
  SET PRESENTACIÓN=2;

-- kilo
SELECT producto FROM productos
WHERE `u/medida`='kilo';

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE `u/medida`='kilo')c1
  USING(producto)
  set PRESENTACIÓN=3;

-- Ejercicio 10
ALTER TABLE productos ADD COLUMN(CATEGORÍA char(1));

-- Inferior a 10$
SELECT producto FROM productos
WHERE importe_base<10;

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE importe_base<10)c1
  USING(producto)
  SET CATEGORÍA='A';

-- Entre 10 y 20
SELECT producto FROM productos
WHERE importe_base BETWEEN 10 AND 20;

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE importe_base BETWEEN 10 AND 20) c1
  USING(producto)
  SET CATEGORÍA='B';

-- Superior a 20
SELECT producto FROM productos
WHERE importe_base>20;

UPDATE productos JOIN(
  SELECT producto FROM productos
  WHERE importe_base>20) c1
  USING(producto)
  set CATEGORÍA='C';

-- Ejercicio 11

-- Frutas y granja litoral
ALTER TABLE productos ADD COLUMN (AUMENTO2 float);

SELECT producto, importe_base, importe_base*0.1 aumento FROM productos
WHERE rubro='frutas' AND granja='litoral';

UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.1 aumento FROM productos
  WHERE rubro='frutas' AND granja='litoral') c1
  USING (producto)
  SET AUMENTO2=aumento;

-- Verduras y granja ceibal
SELECT producto, importe_base, importe_base*0.15 aumento FROM productos
WHERE rubro='verduras' AND granja='el ceibal';

UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.15 aumento FROM productos
  WHERE rubro='verduras' AND granja='el ceibal') c1
  USING(producto)
  SET AUMENTO2=aumento;

-- Semillas y granja el canuto
SELECT producto, importe_base, importe_base*0.2 aumento FROM productos
WHERE rubro='semillas' AND granja='el canuto';

UPDATE productos JOIN(
  SELECT producto, importe_base, importe_base*0.2 aumento FROM productos
  WHERE rubro='semillas' AND granja='el canuto') c1
  USING(producto)
  SET aumento2=aumento;

