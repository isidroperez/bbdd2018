﻿-- Consulta 1 Nombre y edad de los ciclistas que NO han ganado etapas
SELECT nombre, edad FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Consulta 2 Nombre y edad de los ciclistas que NO han ganado puertos
SELECT nombre, edad FROM ciclista LEFT JOIN puerto ON ciclista.dorsal=puerto.dorsal
WHERE puerto.dorsal IS NULL;

-- Consulta 3 Listar el director de los equipos que tengan ciclistas NO que hayan ganado NINGUNTA etapa
SELECT distinct director FROM (equipo INNER JOIN ciclista ON ciclista.nomequipo=equipo.nomequipo) LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Consulta 4 Dorsal y nombre de los ciclistas que no hayan llevado algún maillot
SELECT ciclista.dorsal, nombre FROM ciclista LEFT JOIN lleva ON ciclista.dorsal=lleva.dorsal
WHERE lleva.dorsal IS NULL;

-- Consulta 5 Dorsal y nombre de los ciclistas que NO hayan llevado el maillot amarillo NUNCA
  -- C1 Ciclistas con todos los maillots
    SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista INNER JOIN lleva INNER JOIN maillot ON ciclista.dorsal=lleva.dorsal AND lleva.código=maillot.código;
  -- C2 Ciclistas con maillot amarillo
    SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista INNER JOIN lleva INNER JOIN maillot ON ciclista.dorsal=lleva.dorsal AND lleva.código=maillot.código
    WHERE color= 'Amarillo';
  -- Consulta de resta, definitiva
    SELECT c1.dorsal, c1.nombre FROM (SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista INNER JOIN lleva INNER JOIN maillot ON ciclista.dorsal=lleva.dorsal AND lleva.código=maillot.código) AS c1 LEFT JOIN  (SELECT DISTINCT ciclista.dorsal, nombre FROM ciclista INNER JOIN lleva INNER JOIN maillot ON ciclista.dorsal=lleva.dorsal AND lleva.código=maillot.código WHERE color= 'Amarillo') AS c2
      ON c1.dorsal=c2.dorsal WHERE c2.dorsal IS NULL;

-- Consulta 6 Indicar el numetapa de las etapas que NO tengan puertos.
SELECT etapa.numetapa FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- Consulta 7 Indicar la distancia media de las etapas que NO tengan puertos.
SELECT AVG(kms) FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- Consulta 8 Listar el número de ciclistas que NO hayan ganado alguna etapa.
SELECT COUNT(*) FROM ciclista LEFT JOIN etapa ON ciclista.dorsal=etapa.dorsal
WHERE etapa.dorsal IS NULL;

-- Consulta 9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto.
SELECT distinct ciclista.dorsal FROM ciclista INNER JOIN etapa ON ciclista.dorsal=etapa.dorsal LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
WHERE puerto.numetapa IS NULL;

-- Consulta 10 Listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos.
    -- C1 ciclistas que han ganado etapas que no tienen puerto
  SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
  WHERE puerto.numetapa IS NULL;
   -- C2 ciclistas que han ganado etapas con puertos
  SELECT DISTINCT etapa.dorsal from etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa;
  
   -- Consulta de resta
  SELECT DISTINCT c1.dorsal FROM 
    (SELECT DISTINCT etapa.dorsal FROM etapa LEFT JOIN puerto ON etapa.numetapa=puerto.numetapa
       WHERE puerto.numetapa IS NULL) AS c1
     LEFT JOIN 
    (SELECT DISTINCT etapa.dorsal from etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa) AS c2
    ON c1.dorsal=c2.dorsal
    WHERE c2.dorsal IS NULL;

