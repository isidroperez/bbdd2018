﻿-- Ejercicio 2. Creando la tabla importes.

CREATE OR REPLACE TABLE importes(
  Tipo varchar(15),
  Valor int,
  PRIMARY KEY(Tipo)
  );

-- Insertando valores.
INSERT INTO importes (Tipo, Valor)
  VALUES ('Jornalero', 25), ('Efectivo', 250);

-- Ejercicio 3. Liquidación. Obtener salario básico.
  /* Añadiendo la columna */
  ALTER TABLE empleados DROP COLUMN SalarioBase;
  ALTER TABLE empleados ADD COLUMN (SalarioBase float);
  
  /* Obtener salario*/
  UPDATE empleados JOIN importes ON Tipo_trabajo=Tipo
    SET SalarioBase=
    IF(Tipo_trabajo='Jornalero', Horas_trabajadas*Valor, IF(Tipo_trabajo='Efectivo', Dias_trabajdos*Valor, 0));
  
-- Ejercicio 4. Incremento 15% chófer y azafata. Restantes 5%. >20 días 200 horas.
  /* Añadiendo premios */
  ALTER TABLE empleados DROP COLUMN Premios;
  ALTER TABLE empleados ADD COLUMN Premios float;

  /* Updates */
  UPDATE empleados
  SET Premios=
    CASE
      WHEN (rubro = 'Chofer' OR rubro = 'Azafata') AND (Horas_trabajadas>200 OR Dias_trabajdos>20) THEN Salariobase*0.15
      WHEN (NOT (rubro = 'Chofer' OR rubro = 'Azafata') ) AND (Horas_trabajadas>200 OR Dias_trabajdos>20) THEN Salariobase*0.05
      ELSE 0
      END; 

  -- Ejercicio 5. Calcular Sueldo nominal (sumando premios y salario base).
  /* Añadiendo Sueldo Nominal */
  ALTER TABLE empleados DROP COLUMN SueldoNominal;
  ALTER TABLE empleados ADD COLUMN SueldoNominal float;

  /* Calculando el Sueldo Nominal */
  UPDATE empleados
  SET SueldoNominal=SalarioBase+Premios;

  -- Ejercicio 6. Calcular descuentos respectivos.
  /* Creando BPS */
  ALTER TABLE empleados DROP COLUMN BPS;
  ALTER TABLE empleados ADD COLUMN BPS float;

  /* Creando IRPF */
  ALTER TABLE empleados DROP COLUMN IRPF;
  ALTER TABLE EMPLEADOS ADD COLUMN IRPF float;

  /* Calculando BPS */
  UPDATE empleados
  SET BPS=SueldoNominal*0.13;
  
  /* Calculando IRPF */
  UPDATE empleados
  SET IRPF=
    CASE
      WHEN SueldoNominal<(4*1160) THEN SueldoNominal*0.03
      WHEN SueldoNominal BETWEEN(4*1160) AND (10*1160) THEN SueldoNominal*0.06
      WHEN SueldoNominal>(10*1160) THEN SueldoNominal*0.09
      END;

  -- Ejercicio 7. Calculando todos los descuentos.
  ALTER TABLE empleados DROP COLUMN SubtotalDescuentos;
  ALTER TABLE empleados ADD COLUMN SubtotalDescuentos float;

  UPDATE empleados
  SET SubtotalDescuentos=BPS+IRPF;

  -- Ejercicio 8. Calculando el sueldo líquido.
  ALTER TABLE empleados DROP COLUMN SueldoLiquido;
  ALTER TABLE empleados ADD COLUMN SueldoLiquido float;

  UPDATE empleados
  SET SueldoLiquido=SueldoNominal-SubtotalDescuentos;

  -- Ejercicio 9. Agregar tabla V-Transporte.
  ALTER TABLE empleados DROP COLUMN VTransporte;
  ALTER TABLE empleados ADD COLUMN VTransporte float;

  UPDATE empleados
  SET VTransporte=
    CASE 
      WHEN year(NOW())-year(`Fecha-nac`)>40 AND Barrio NOT LIKE 'Cordon' AND Barrio NOT LIKE 'Centro' THEN 200
      WHEN year(NOW())-year(`Fecha-nac`)>40 AND Barrio LIKE 'Cordon' OR barrio LIKE 'Centro' THEN 150
      WHEN year(NOW())-year(`Fecha-nac`)<40 AND barrio NOT LIKE 'Cordon' AND barrio NOT LIKE 'Centro' THEN 100
      ELSE 0
      END;

  -- Ejercicio 10. Agregar columna V-Alimentacion.
  ALTER TABLE empleados DROP COLUMN VAlimentacion;
  ALTER TABLE empleados ADD COLUMN VAlimentacion float;

  UPDATE empleados
  SET VAlimentacion=
    CASE
      WHEN SueldoLiquido<5000 THEN 300
      WHEN SueldoLiquido BETWEEN 5000 AND 10000 AND Tipo_trabajo='Jornalero' THEN 200
      WHEN SueldoLiquido BETWEEN 5000 AND 10000 AND Tipo_trabajo='Efectivo' THEN 100
      WHEN SueldoLiquido>1000 THEN 0
      END;


SELECT * FROM empleados;