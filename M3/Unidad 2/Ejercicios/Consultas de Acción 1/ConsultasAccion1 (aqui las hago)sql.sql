﻿-- 3.-
UPDATE productos
SET cantidad=0;

UPDATE venden
SET total=0;

UPDATE cliente
SET cantidad=0, nombreCompleto=0;

-- 4.-
  -- c1
SELECT precio p, venden.cantidad c FROM productos JOIN venden ON id=producto;
-- actualizacion
  UPDATE venden JOIN(
SELECT precio p, venden.cantidad c FROM productos JOIN venden ON id=producto
  ) c1 ON c=cantidad
SET total=(SELECT c1.p*c1.c);

SELECT * FROM venden;

  -- c2
SELECT CONCAT_WS(" ",nombre,apellidos) FROM cliente;

UPDATE cliente
SET nombreCompleto=CONCAT_WS(" ",nombre,apellidos);
  
-- 5.-
  -- a
    -- c1
    SELECT precio p, venden.cantidad c, descuento d FROM productos JOIN venden ON id=producto JOIN cliente ON cliente.id=cliente;

    -- Accion
    UPDATE venden JOIN(
       SELECT cliente.id i1,productos.id i2, venden.cantidad c, precio p, descuento d FROM productos JOIN venden ON id=producto JOIN cliente ON cliente.id=cliente)
      c1 ON c1.i1=venden.cliente AND venden.producto=c1.i2
    SET total=c1.p*c1.c*(1-c1.d/100);


    

