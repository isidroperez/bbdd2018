﻿DROP DATABASE IF EXISTS consultasaccion1;
CREATE DATABASE IF NOT EXISTS consultasaccion1;
USE consultasaccion1;

CREATE OR REPLACE TABLE productos(
  id int AUTO_INCREMENT,
  grupo varchar(15),
  peso int,
  nombre varchar(15),
  precio float,
  cantidad int,
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE cliente(
  id int AUTO_INCREMENT,
  nombre varchar(15),
  apellidos varchar(15),
  descuento float,
  cantidad int,
  nombreCompleto varchar(15),
  PRIMARY KEY(id)
  );

CREATE OR REPLACE TABLE venden(
  cantidad int,
  total float,
  fecha date,
  producto int,
  cliente int,
  CONSTRAINT FKVendenProducto FOREIGN KEY(producto)
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKVendenCliente FOREIGN KEY(cliente)
  REFERENCES cliente(id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY(producto, cliente)
  );
