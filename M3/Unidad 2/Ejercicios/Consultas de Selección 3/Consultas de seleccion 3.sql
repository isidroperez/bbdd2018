﻿-- Consulta 1
SELECT DISTINCT edad FROM ciclista
WHERE nomequipo = 'Banesto';

-- Consulta 2
SELECT DISTINCT edad FROM ciclista
WHERE nomequipo = 'Banesto' OR nomequipo = 'Navigare';

-- Consulta 3
SELECT dorsal FROM ciclista
WHERE nomequipo = 'Banesto' AND edad BETWEEN 25 AND 32;

-- Consulta 4
SELECT dorsal FROM ciclista
WHERE nomequipo = 'Banesto' OR edad BETWEEN 25 AND 32;

-- Consulta 5
SELECT DISTINCT LEFT(nomequipo, 1) FROM ciclista
WHERE nombre LIKE 'R%';

SELECT DISTINCT LEFT (c.nomequipo, 1) FROM ciclista c WHERE LEFT (c.nombre,1)='R'; -- Esta también sería válida y más correcta.
-- Consulta 6
SELECT numetapa FROM etapa
WHERE salida = llegada;

-- Consulta 7
SELECT numetapa FROM etapa
WHERE salida NOT like llegada AND dorsal IS NOT NULL;

-- Consulta 8
SELECT nompuerto FROM puerto
WHERE altura BETWEEN 1000 AND 2000 OR altura > 2400;

-- Consulta 9
SELECT dorsal FROM puerto
WHERE altura BETWEEN 1000 AND 2000 OR altura > 2400;

-- Consulta 10
SELECT COUNT(DISTINCT dorsal) FROM etapa e;

-- Consulta 11
SELECT COUNT(*) FROM etapa INNER JOIN puerto
WHERE puerto.numetapa = etapa.numetapa;

-- Consulta 12
SELECT COUNT(*) FROM puerto INNER JOIN ciclista c
WHERE c.dorsal=puerto.dorsal;

-- Consulta 13
SELECT etapa.numetapa, COUNT(nompuerto) NumeroDeEtapas FROM etapa INNER JOIN puerto
WHERE etapa.numetapa = puerto.numetapa
GROUP BY etapa.numetapa;

SELECT etapa.numetapa, COUNT(nompuerto) FROM etapa INNER JOIN puerto ON etapa.numetapa=puerto.numetapa
GROUP BY etapa.numetapa; -- Esta es más correcta

-- Consulta 14
SELECT AVG(altura) FROM puerto;

-- Consulta 15
SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura) > 1500;

-- Consulta 16
-- C1
SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura) > 1500;

-- Consulta
SELECT COUNT(*) PuertosMayor1500 FROM (
SELECT numetapa FROM puerto GROUP BY numetapa HAVING AVG(altura) > 1500) AS C1;

-- Consulta 17
SELECT l.dorsal,COUNT(DISTINCT l.código) FROM lleva l GROUP BY l.dorsal;

-- Consulta 18
SELECT DISTINCT dorsal, código, COUNT(*) NumeroDeMaillots FROM lleva
GROUP BY dorsal, código;

-- Consulta 19
SELECT numetapa, dorsal, COUNT(código) FROM lleva
GROUP BY numetapa,dorsal;

SELECT
c.nombre Nombre ,C1.*
FROM (
    SELECT numetapa, dorsal, COUNT(código) FROM lleva
    GROUP BY numetapa,dorsal) AS C1
INNER JOIN
ciclista c 
ON
c.dorsal=C1.dorsal;

-- Consultas de clase, joins

-- inner join con using

SELECT * FROM ciclista c INNER JOIN puerto p USING(dorsal);

-- inner join con on

SELECT * FROM ciclista c INNER JOIN puerto p ON c.dorsal=p.dorsal;

SELECT * FROM ciclista NATURAL JOIN puerto;

-- unir las tablas equipo, ciclista y etapa

SELECT * FROM equipo JOIN ciclista JOIN etapa ON ciclista.nomequipo=equipo.nomequipo AND ciclista.dorsal=etapa.dorsal;

SELECT * FROM equipo JOIN ciclista ON ciclista.nomequipo=equipo.nomequipo JOIN etapa ON ciclista.dorsal=etapa.dorsal;

SELECT * FROM equipo JOIN ciclista USING(nomequipo) JOIN etapa USING(dorsal);
