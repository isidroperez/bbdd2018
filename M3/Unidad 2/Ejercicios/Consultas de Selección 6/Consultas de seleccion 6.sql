﻿-- Consulta 1 : Numero de vendedores cuya fecha de alta sea en febrero de cualquier año
SELECT COUNT(*) FROM vendedores
WHERE MONTH(FechaAlta)=2;

-- Consulta 2 : Numero de vendedores guapos y feos
-- Feos
SELECT COUNT(*) FROM vendedores
WHERE `Guap@` IS FALSE;
-- Guapos
SELECT COUNT(*)FROM vendedores
WHERE `Guap@` IS TRUE;
-- Suma
SELECT (SELECT COUNT(*)FROM vendedores
WHERE `Guap@` IS TRUE)+(SELECT COUNT(*) FROM vendedores
WHERE `Guap@` IS FALSE);

-- Consulta 3 : El nombre del producto más caro
-- Subconsulta max precio
SELECT MAX(Precio) FROM productos;
-- Consulta
SELECT NomProducto FROM productos
WHERE Precio=(SELECT MAX(Precio) FROM productos);

-- Consulta 4 : El precio medio de los productos por grupo
SELECT IdGrupo, AVG(Precio) FROM productos
GROUP BY IdGrupo;

-- Consulta 5 : Indica que grupo tiene producto que se hayan vendido alguna vez
SELECT distinct IdGrupo FROM ventas JOIN productos ON IdProducto=`Cod Producto`;

-- Consulta 6 : Indica cuales son los grupos de los cuales no se ha vendido ningún producto
SELECT distinct IdGrupo FROM ventas left JOIN productos ON IdProducto=`Cod Producto`
WHERE `Cod Producto` IS NULL;

-- Consulta 7 : Numero de poblaciones cuyos vendedores son guapos
SELECT COUNT(DISTINCT Poblacion) FROM vendedores
WHERE `Guap@` IS TRUE;

-- Consulta 8 : Nombre de la población que tiene más vendedores casados
-- C1
SELECT
  Poblacion, COUNT(*) 
FROM 
  vendedores
WHERE 
  EstalCivil = 'casado'
GROUP BY 
  Poblacion;

-- C2
SELECT
  poblacion 
FROM 
  vendedores
WHERE 
  EstalCivil = 'casado'
GROUP BY
  Poblacion
HAVING COUNT(*)
  =(SELECT MAX(n) FROM 
     (SELECT Poblacion, COUNT(*) n FROM vendedores
      WHERE EstalCivil = 'casado'
      GROUP BY Poblacion) c1);

-- Consulta 9 : Población que ninguno de sus vendedores están casados
-- c1
SELECT DISTINCT Poblacion FROM vendedores
WHERE EstalCivil like 'Casado';

-- c2
SELECT DISTINCT poblacion FROM vendedores
WHERE EstalCivil NOT LIKE 'Casado';

-- Consulta
SELECT c1.Poblacion FROM (SELECT DISTINCT Poblacion FROM vendedores
WHERE EstalCivil NOT like 'Casado') c1 LEFT JOIN (SELECT DISTINCT poblacion FROM vendedores
WHERE EstalCivil LIKE 'Casado') c2 ON c1.Poblacion=c2.Poblacion
WHERE c2.Poblacion IS NULL;

-- Consulta 10 : Vendedor que no ha vendido nada
SELECT NombreVendedor FROM vendedores LEFT JOIN ventas ON IdVendedor=`Cod Vendedor`
WHERE `Cod Vendedor` IS NULL;

-- Consulta 11 : Vendedor que ha vendido más kilos
SELECT MAX(kilos) FROM ventas;

SELECT NombreVendedor FROM ventas JOIN vendedores ON IdVendedor=`Cod Vendedor`
WHERE kilos=(SELECT MAX(kilos) FROM ventas);