﻿DROP DATABASE IF EXISTS hoja7ejercicio6;
CREATE DATABASE hoja7ejercicio6;
USE hoja7ejercicio6;

CREATE OR REPLACE TABLE opcion(
  nombre varchar(15),
  des varchar(15),
  PRIMARY KEY (nombre)
  );

CREATE OR REPLACE TABLE modelo(
  marca varchar(15),
  cil varchar(15),
  modelo varchar(15),
  precio varchar(15),
  PRIMARY KEY(marca, cil, modelo)
  );

CREATE OR REPLACE TABLE tiene(
  marca varchar(15),
  cil varchar (15),
  modelo varchar(15),
  nombreopcion varchar(15),
  precio varchar(15),
  CONSTRAINT FKTieneMarcaCilModelo FOREIGN KEY(marca, cil, modelo)
  REFERENCES modelo(marca) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTieneOpcion FOREIGN KEY(nombreopcion)
  REFERENCES opcion(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY(marca, cil, modelo)
  );

CREATE OR REPLACE TABLE vendedor(
  dni varchar(9),
  nombre varchar(15),
  dir varchar (15),
  tel varchar(15),
  PRIMARY KEY(dni)
  );

CREATE OR REPLACE TABLE cliente(
  dni varchar(9),
  nombre varchar(15),
  dir varchar(30),
  tel varchar(9),
  PRIMARY KEY(dni)
  );

CREATE OR REPLACE TABLE compra(
  nomopcion varchar(15),
  marca varchar (15),
  cil varchar (15),
  modelo varchar(15),
  vendedor varchar(9),
  cliente varchar(9),
  mat varchar(9),
  fecha date,
  CONSTRAINT FKCompraOpcion FOREIGN KEY (nomopcion)
  REFERENCES opcion(nombre) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraModelo FOREIGN KEY (marca, cil, modelo)
  REFERENCES modelo(marca,cil,modelo) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraVendedor FOREIGN KEY(vendedor)
  REFERENCES vendedor(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKCompraCliente FOREIGN KEY (cliente)
  REFERENCES cliente(dni) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY(nomopcion, marca, cil, modelo, vendedor, cliente)
  );

 CREATE OR REPLACE TABLE vehiculo(
  mat varchar(7),
  precio varchar(5),
  marca varchar(15),
  modelo varchar(15),
  PRIMARY KEY (mat)
  );

CREATE OR REPLACE TABLE cede(
  cliente varchar(9),
  mat varchar(7),
  fecha date,
  CONSTRAINT UNIQUEMat UNIQUE KEY(mat),
  CONSTRAINT FKCedeMat FOREIGN KEY(mat)
  REFERENCES vehiculo(mat) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKcedeCliente FOREIGN KEY(cliente)
  REFERENCES cliente(dni) ON DELETE CASCADE ON UPDATE CASCADE
  );