﻿-- Hoja de ejercicios 7 - unidad 1 - módulo 3.

-- Eliminar la base de datos si existe.
DROP DATABASE IF EXISTS hoja7unidad1modulo3;

-- Crear la base de datos si no existe.
CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3;

-- Utilizar la base de datos.
USE hoja7unidad1modulo3;

-- Crear tabla.
CREATE TABLE Empleado(
    dni char(9),
    PRIMARY KEY (dni)
  );

INSERT INTO empleado VALUES
  ('dni1'),
  ('dni2');

CREATE TABLE departamento(
   Cod_dpto char(15),
   PRIMARY KEY (Cod_dpto)
  );

INSERT INTO Departamento VALUES
 ('departamento1'),
 ('departamento2');

DROP TABLE IF EXISTS pertenece;

CREATE TABLE pertenece(
  departamento char(15),
  empleado char(9),
  PRIMARY KEY (departamento, empleado),
  CONSTRAINT uniqueEmpleado UNIQUE KEY (empleado),
  CONSTRAINT FKPerteneceEmpleado FOREIGN KEY (empleado)
    REFERENCES empleado(dni) on DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT FKPerteneceDepartamento FOREIGN KEY(departamento)
    REFERENCES Departamento(Cod_dpto) on DELETE CASCADE on UPDATE CASCADE
 );

CREATE TABLE proyecto(
  cod_proy varchar(15),
  PRIMARY KEY(cod_proy)
);

INSERT INTO proyecto VALUES
 ('Proyecto1'),
 ('Proyecto2');

DROP TABLE IF EXISTS trabaja;

CREATE TABLE trabaja(
    empleado char(9),
    cod_proy varchar(15),
    PRIMARY KEY (empleado, cod_proy),
    CONSTRAINT FKTrabajaEmpleado FOREIGN KEY (empleado)
      REFERENCES empleado(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKTrabajaProyecto FOREIGN KEY(cod_proy)
      REFERENCES proyecto(cod_proy) ON DELETE CASCADE ON UPDATE CASCADE
  );




-- Ejercicio 2
DROP DATABASE IF EXISTS Hoja7Ejercicio2;

CREATE DATABASE IF NOT EXISTS Hoja7Ejercicio2;

USE Hoja7Ejercicio2;

CREATE TABLE SOLDADO(
  s char(3),
  grado varchar(15),
  nombre varchar(15),
  apellidos varchar(15),
  PRIMARY KEY (s)
  );

INSERT INTO SOLDADO (s, grado, nombre, apellidos)
  VALUES 
('001', 'Grado1', 'Nombre1', 'Apellido1'),
('002', 'Grado2', 'Nombre2', 'Apellido2'),
('003', 'Grado3', 'Nombre3', 'Apellido3');

CREATE TABLE COMPAÑIA(
  n char(3),
  actividad varchar(15),
  PRIMARY KEY(n)
  );

INSERT INTO COMPAÑIA (n, actividad)
  VALUES
 ('001', 'actividad1'),
 ('002', 'actividad2'),
 ('003', 'actividad3');

DROP TABLE IF EXISTS pertenece;

CREATE TABLE IF NOT EXISTS pertenece(
  n char(3),
  s char(3),
  PRIMARY KEY (n, s),
  CONSTRAINT FKPerteneceCompañia FOREIGN KEY (n)
  REFERENCES COMPAÑIA(n) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKPerteneceSoldado FOREIGN KEY (s)
  REFERENCES SOLDADO(s) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE SERVICIO(
  se char(3),
  des varchar(15),
  PRIMARY KEY (se)
  );

INSERT INTO SERVICIO (se, des)
  VALUES 
('001', 'des1'),
('002', 'des2'),
('003', 'des3');

DROP TABLE IF EXISTS realiza;
CREATE TABLE IF NOT EXISTS realiza(
  s char(3),
  se char(3),
  fecha varchar(15),
  PRIMARY KEY(s, se),
  CONSTRAINT FKRealizaSoldado FOREIGN KEY (s)
  REFERENCES SOLDADO(s) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKRealizaServicio FOREIGN KEY (se)
  REFERENCES SERVICIO(se) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE CUERPO(
  c char(3),
  deno varchar(15),
  PRIMARY KEY(c)
  );

INSERT INTO CUERPO (c, deno)
  VALUES 
('001', 'deno1'),
('002', 'deno2'),
('003', 'deno3');

DROP TABLE IF EXISTS pertenece1;
CREATE TABLE IF NOT EXISTS pertenece1(
  s char(3),
  c char(3),
  PRIMARY KEY(s, c),
  CONSTRAINT FKPertenece1Soldado FOREIGN KEY(s)
  REFERENCES SOLDADO(s) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKPertenece1Cuerpo FOREIGN KEY(c)
  REFERENCES CUERPO(c) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE CUARTEL(
  cu char(3),
  nombre varchar(15),
  dir varchar(15),
  PRIMARY KEY(cu)
  );

INSERT INTO CUARTEL (cu, nombre, dir)
  VALUES 
('001', 'nombre1', 'dir1'),
('002', 'nombre2', 'dir2'),
('003', 'nombre3', 'dir3');

DROP TABLE IF EXISTS esta;
CREATE TABLE IF NOT EXISTS esta(
  s char(3),
  cu char(3),
  PRIMARY KEY (s, cu),
  CONSTRAINT FKEstaSoldado FOREIGN KEY(s)
  REFERENCES SOLDADO(s) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKEstaCuartel FOREIGN KEY(cu)
  REFERENCES CUARTEL(cu) ON DELETE CASCADE ON UPDATE CASCADE
  );



-- Ejercicio 3
DROP DATABASE IF EXISTS Hoja7Ejercicio3;

CREATE DATABASE IF NOT EXISTS Hoja7Ejercicio3;

USE Hoja7Ejercicio3;

CREATE TABLE TURISTA(
  t char(3),
  dir varchar(15),
  nombre varchar(15),
  apellidos varchar(15),
  tel varchar(15),
  PRIMARY KEY (t)
  );

CREATE TABLE HOTEL(
  h char(3),
  nombre varchar(15),
  dir varchar(15),
  ciudad varchar(15),
  plazas int,
  tel varchar(15),
  PRIMARY KEY(h)
  );


DROP TABLE IF EXISTS reserva;

CREATE TABLE IF NOT EXISTS reserva(
  t char(3),
  h char(3),
  PRIMARY KEY (t, h),
  CONSTRAINT FKResrvaTurista FOREIGN KEY (t)
  REFERENCES TURISTA(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKReservaHotel FOREIGN KEY (h)
  REFERENCES HOTEL(h) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE VUELO(
  n char(3),
  fecha varchar(15),
  hora varchar(15),
  origen varchar(15),
  destino varchar(15),
  nturista varchar(15),
  ntotal varchar(15),
  PRIMARY KEY (n)
  );


DROP TABLE IF EXISTS toma;
CREATE TABLE IF NOT EXISTS toma(
  t char(3),
  n char(3),
  clase varchar(15),
  PRIMARY KEY(t, n),
  CONSTRAINT FKTomaTurista FOREIGN KEY (t)
  REFERENCES TURISTA(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKTomaVuelo FOREIGN KEY (n)
  REFERENCES VUELO(n) ON DELETE CASCADE ON UPDATE CASCADE
  );

CREATE TABLE AGENCIA(
  s char(3),
  direccion varchar(15),
  tel varchar(15),
  PRIMARY KEY(s)
  );


DROP TABLE IF EXISTS contrata;
CREATE TABLE IF NOT EXISTS contrata(
  t char(3),
  s char(3),
  PRIMARY KEY(t, s),
  CONSTRAINT FKContrataTurista FOREIGN KEY(t)
  REFERENCES TURISTA(t) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKContrataAgencia FOREIGN KEY(s)
  REFERENCES AGENCIA(s) ON DELETE CASCADE ON UPDATE CASCADE
  );
