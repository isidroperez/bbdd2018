﻿SELECT e.emp_no,
       e.apellido,
       e.oficio,
       e.dir,
       e.fecha_alt,
       e.salario,
       e.comision,
       e.dept_no
  FROM empleado e
  GROUP BY e.apellido DESC
