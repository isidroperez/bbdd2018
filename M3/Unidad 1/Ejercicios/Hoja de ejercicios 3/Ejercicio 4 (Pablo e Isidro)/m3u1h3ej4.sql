﻿DROP DATABASE IF EXISTS m3u1h3ej4;
CREATE DATABASE IF NOT EXISTS m3u1h3ej4;
USE m3u1h3ej4;

CREATE OR REPLACE TABLE Banco(
  codBanco varchar(15),
  domBanco varchar(15),
  PRIMARY KEY (nombre)
  );

CREATE OR REPLACE TABLE sucursal(
  Cod varchar(15),
  Domicilio varchar(15),
  N_Empleados varchar(15),
  PRIMARY KEY(Cod)
  );

CREATE OR REPLACE TABLE pertenece(
  Banco varchar(15),
  Sucursal varchar (15),
  CONSTRAINT FKPerteneceBanco FOREIGN KEY(Banco)
  REFERENCES Banco(codBanco) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKPerteneceSucursal FOREIGN KEY(Sucursal)
  REFERENCES sucursal(Cod) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT UNICASucursalL UNIQUE(Sucursal),
  PRIMARY KEY(Banco, Sucursal)
  );

CREATE OR REPLACE TABLE vigilante(
  CodVig varchar(15),
  Edad varchar(15),
  PRIMARY KEY(CodVig)
  );

CREATE OR REPLACE TABLE contrata(
  Sucursal varchar(15),
  Vigilante varchar(15),
  Fecha date,
  CONSTRAINT FKContrataSucursal FOREIGN KEY(Sucursal)
  REFERENCES sucursal(Cod) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKContrataVigilante FOREIGN KEY(Vigilante)
  REFERENCES vigilante(CodVig) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT UNICAVigilante UNIQUE(Vigilante),
  PRIMARY KEY(Sucursal, Vigilante)
  );

CREATE OR REPLACE TABLE atracador(
  Clave varchar(15),
  Nombre varchar(15),
  PRIMARY KEY (Clave)
  );

 CREATE OR REPLACE TABLE detiene(
  Sucursal varchar(15),
  Atracador varchar(15),
  Fecha date,
  CONSTRAINT FKDetieneSucursal FOREIGN KEY(Sucursal)
  REFERENCES sucursal(cod) on DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT FKDetieneAtracador FOREIGN KEY(Atracador)
  REFERENCES atracador(Clave) on DELETE CASCADE on UPDATE CASCADE,
  CONSTRAINT UNICAAtracador UNIQUE(Atracador),
  PRIMARY KEY(Sucursal, Atracador)
  );

CREATE OR REPLACE TABLE juez(
  Clave varchar(15),
  nombre varchar(15),
  Anos_servicio varchar(15),
  PRIMARY KEY (Clave)
  );

CREATE OR REPLACE TABLE juzgado(
  Atracador varchar(15),
  Juez varchar(15),
  Condena bool,
  Anos int,
  CONSTRAINT FKJuzgadoAtracador FOREIGN KEY(Atracador)
  REFERENCES atracador(Clave) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKJuzgadoJuez FOREIGN(Juez)
  REFERENCES juez(Clave) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT UNICAAtracador UNIQUE(Atracador),
  PRIMARY KEY(Atracador, Juez)
  );

CREATE OR REPLACE TABLE banda(
  N_Banda varchar(15),
  N_Personas varchar(15),
  PRIMARY KEY(N_Banda)
  );

CREATE OR REPLACE TABLE pertenece2(
  Banda varchar(15),
  Atracador varchar(15),
  CONSTRAINT FKPertenece2Banda FOREIGN KEY(Banda)
  REFERENCES Banda(N_Banda) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FKPertenece2Atracador FOREIGN KEY(Atracador)
  REFERENCES atracador(Clave) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT UNICAAtracador UNIQUE(Atracador),
  PRIMARY KEY(Banda, Atracador)
  );
