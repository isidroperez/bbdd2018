﻿CREATE DATABASE ejercicioclase;
USE ejercicioclase;

CREATE OR REPLACE TABLE pelicula(
  Id int AUTO_INCREMENT PRIMARY KEY,
  Titulo varchar(20),
  FechaSalida date,
  Duracion int,
  Genero varchar(20)
  );

CREATE OR REPLACE TABLE sala(
  Id int AUTO_INCREMENT PRIMARY KEY,
  Localidad varchar(20),
  Provincia varchar(20),
  Capacidad int,
  email varchar(20)
  );

CREATE OR REPLACE TABLE proyecta(
  idsala int,
  idpelicula int,
  hora time);

TRUNCATE pelicula;
INSERT INTO pelicula VALUES (DEFAULT, 'Alita', 2019-02-26, 95, 'Acción'), 
  (DEFAULT, 'Bohemian Rhapsody', 2018-07-15, 105, 'Musical'),
  (DEFAULT, 'SAW VII', 2018-03-26, 95, 'Terror'),
  (DEFAULT, 'SuperLópez', 2019-01-15, 95, 'Comedia'),
  (DEFAULT, 'La lista de Schindler', 2019-02-26, 95, 'Drama'),
  (DEFAULT, 'Dragon Ball SuperBroly', 2019-03-04, 95, 'Animación'),
  (DEFAULT, 'Marwen', 2019-02-11, 95, 'Comedia');

INSERT INTO sala VALUES
  (DEFAULT, 'Potes', 'Cantabria', 130, null),
  (DEFAULT, 'Santander', 'Cantabria', 100, NULL),
  (DEFAULT, 'Torrelavega', 'Cantabria', 170, NULL),
  (DEFAULT, 'Corrales', 'Cantabria', 150, null);


/*                    PROCEDIMIENTOS              */

DELIMITER //
CREATE OR REPLACE PROCEDURE p1(idsala int, idpeli int, hora time)
  COMMENT 'Este procedimiento insertará de forma automática la hora de proyección, película y en qué sala estará introduciendo parámetros'
  BEGIN
  INSERT INTO proyecta VALUES (idsala, idpeli, hora);
  SELECT CONCAT('La película ', Titulo,' ','será proyectada a las ',' ',hora,' ','en la sala ', idsala) FROM pelicula
  WHERE Id=idpeli;
  END//
DELIMITER ;

CALL p1(3, 2, '15:00:00');
SELECT * FROM proyecta;

DELIMITER //
CREATE OR REPLACE PROCEDURE p2(idsala int)
COMMENT 'Este procedimiento insertará a partir de la función creada el email creado en la tabla'
BEGIN
UPDATE sala
  SET email=(SELECT crearemail(idsala))
  WHERE Id=idsala;
END//
DELIMITER ;
CALL p2(1);
SELECT * FROM sala;


DELIMITER //
CREATE OR REPLACE PROCEDURE p3(numeroentradas int, idsala int)
  COMMENT 'Este procedimiento actualizará el campo capacidad en función de las entradas compradas'
  BEGIN

  
    
  IF    (SELECT capacidad FROM sala
        WHERE Id=idsala)<numeroentradas THEN
        SIGNAL SQLSTATE '45000' set MESSAGE_TEXT='No hay suficientes entradas';
  
  ELSE  
    UPDATE sala
        SET Capacidad=Capacidad-numeroentradas
        WHERE Id=idsala;

  END IF;
  
  END//
DELIMITER ;

CALL p3(10,1);
SELECT * FROM sala;


/*              FUNCIONES             */
-- La primera función devolverá el email de la sala
DELIMITER //
CREATE OR REPLACE FUNCTION crearemail(idsala int, localidad varchar(20))
  RETURNS varchar(20)
  BEGIN
  DECLARE v1 varchar(20);
  set v1=CONCAT(Localidad,idsala,'@cinesa.com'); 
  
  RETURN v1;
  END //
  DELIMITER ;
SELECT crearemail(1,'sss');

SELECT * FROM sala;

-- Función para ver localidades libres de una sala.
DELIMITER //
CREATE OR REPLACE FUNCTION localidadeslibres(idsala int)
  RETURNS int
  BEGIN
  DECLARE v1 int;
  SELECT capacidad INTO v1 FROM sala WHERE Id=idsala;
  RETURN v1;
  END //
DELIMITER ;
SELECT Localidadeslibres(4);

-- Función para ver el número de películas que proyecta una sala ese día.
  DELIMITER //
CREATE OR REPLACE FUNCTION cartelera(sala int)
  RETURNS int
  BEGIN
  DECLARE v1 int;
  SELECT COUNT(*) INTO v1 FROM proyecta WHERE idsala=sala;
  RETURN v1;
  END //
DELIMITER ;

SELECT cartelera(4);


/*          DISPARADORES          */
DELIMITER //
CREATE OR REPLACE TRIGGER t1
  /* Este disparador introducirá automáticamente el email de la sala al introducir un registro */
  BEFORE INSERT
  ON sala
  FOR EACH ROW
  BEGIN
  
    SET new.email=crearemail((SELECT COUNT(*) FROM sala)+1, new.Localidad);
  END //
DELIMITER ;

INSERT INTO sala(Id, Localidad) VALUES (DEFAULT, 'Suances');
SELECT * FROM sala;



DELIMITER //
CREATE OR REPLACE TRIGGER t2
  /* Este disparador eliminará de la tabla proyecta aquellas que se proyecten en la sala que se ha borrado */
  AFTER DELETE 
  ON sala
  FOR EACH ROW
  BEGIN
  DECLARE numero int;
  SELECT COUNT(*) INTO numero FROM proyecta WHERE idsala=old.Id;
  IF (numero)>0 
    THEN
    delete FROM proyecta WHERE OLD.Id=idsala;
    END IF;
   END //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE TRIGGER t3
  /* Este disparador actualizará en la tabla proyecta el Id en caso de que se modifique en la tabla películas */
  BEFORE UPDATE
  ON pelicula
  FOR EACH ROW
  BEGIN
  UPDATE proyecta
  SET idpelicula=NEW.Id WHERE idpelicula=old.Id;
  END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE TRIGGER t4
  /* Este disparador actualizará en la tabla proyecta el Id en caso de que se modifique en la tabla salas */
  BEFORE UPDATE
  ON sala
  FOR EACH ROW
  BEGIN
  UPDATE proyecta
  SET idsala=NEW.id WHERE idsala=old.id;
  END //
DELIMITER ;