﻿﻿/* hoja de ejercicios 2 */

-- ejercicio 1
DELIMITER //
CREATE PROCEDURE ejemploProcedimiento1     /* nombre */
(IN argumento1 integer)                    /* argumentos */
BEGIN                                       /* start of block */
  DECLARE variable1 char(10);               /* variables */
  IF (argumento1=17) THEN                   /* comienzo del IF */
  SET variable1='pajaros';                  /* asignacion */
  ELSE
  SET variable1='leones';                    /* asignacion */
  END IF;                                    /* fin del IF */
  CREATE TABLE table1(
  nombre varchar(20)
  );                                          /* instruccion del LDD */
  INSERT INTO table1 VALUES (variable1);      /* instrucciones de LMD */
  END //                                      
DELIMITER;

-- ejercicio 2
CALL ejemploProcedimiento1();               -- da error porque no hay valores

-- ejercicio 3
CALL ejemploProcedimiento1(17);               -- da error porque ya tiene la tabla creada

-- ejercicio 4
DELIMITER //
CREATE PROCEDURE ejemploProcedimiento2     /* nombre */
(IN argumento1 integer)                    /* argumentos */
BEGIN                                       /* start of block */
  DECLARE variable1 char(10);               /* variables */
  IF (argumento1=17) THEN                   /* comienzo del IF */
  SET variable1='pajaros';                  /* asignacion */
  ELSE
  SET variable1='leones';                    /* asignacion */
  END IF;                                    /* fin del IF */
  CREATE TABLE IF NOT EXISTS table1(
  nombre varchar(20)
  );                                          /* instruccion del LDD */
  INSERT INTO table1 VALUES (variable1);      /* instruccion del LMD */
  END  //                                      
DELIMITER;      

-- ejercicio 5
CALL ejemploProcedimiento2(10);
SELECT * FROM table1;

-- ejercicio 6
DELIMITER //
CREATE PROCEDURE ejemploProcedimiento3     /* nombre */
()                                         /* argumentos */
BEGIN                                       /* start of block */
  SELECT 'Hola mundo';
  END  //                                      
DELIMITER;

CALL ejemploProcedimiento3();

-- ejercicio 9
CREATE PROCEDURE ejemploProcedimiento4(IN arg1 int)
BEGIN
  DECLARE contador int DEFAULT 0;

REPEAT
  set contador=contador+1;
  INSERT INTO table1 VALUES ('Hola mundo');
  UNTIL (contador=arg1)
  END REPEAT;
END;

CALL ejemploProcedimiento4(2);
SELECT * FROM table1;
