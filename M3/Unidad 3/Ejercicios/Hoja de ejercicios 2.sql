﻿ /* hoja de ejercicios 2 */

/* ejercicio 3: Realizar un procedimiento almacenado que visualice todas las personas cuyo salario sea superior a un valor que se
pasara como parámetro de entrada */
DELIMITER //
  DROP PROCEDURE IF EXISTS ej3;

  CREATE PROCEDURE ej3(IN numero int)
  BEGIN
    SELECT apellidos FROM personas WHERE salario>numero;
  END //
    DELIMITER;

CALL ej3(903);

/* ejercicio 4: Realizar un procedimiento almacenado que visualice todas las personas cuyo salario este entre dos valores que se
pasaran como parámetros de entrada */
DELIMITER //
  DROP PROCEDURE IF EXISTS ej4;

  CREATE PROCEDURE ej4(IN num1 int,IN num2 int)
  BEGIN
    SELECT * FROM personas WHERE salario BETWEEN num1 AND num2;
  END //
    DELIMITER;

CALL ej4(933,1333);

/* ejercicio 5: Realizar un procedimiento almacenado que indique cuantos médicos trabajan en un hospital cuyo código se pase
como parámetro de entrada. Además al procedimiento se le pasara un argumento donde se debe almacenar el
número de plazas de ese hospital */
DELIMITER //
DROP PROCEDURE IF EXISTS ej5;

CREATE PROCEDURE ej5(IN numero int)
BEGIN
    DROP TABLE IF EXISTS tabla5;
    CREATE TEMPORARY TABLE tabla5(
      id int AUTO_INCREMENT PRIMARY KEY,
      numMedicos int,
      plazasHospital int
      );

    SELECT COUNT(*) numMedicos FROM medicos WHERE cod_hospital=numero;

    SELECT num_plazas n FROM hospitales GROUP BY cod_hospital HAVING cod_hospital=numero;

    INSERT INTO tabla5 VALUES (DEFAULT,
  (SELECT COUNT(*) FROM medicos WHERE cod_hospital=numero),(
  SELECT num_plazas n FROM hospitales GROUP BY cod_hospital HAVING cod_hospital=numero)
  );

    SELECT * FROM tabla5;
END //
DELIMITER;

CALL ej5(2);


/* ejercicio 6: Crear una función que calcule el volumen de una esfera cuyo radio de tipo FLOAT se pasara como parámetro. Realizar
después una consulta para calcular el volumen de una esfera de radio 5 */

-- v=4/3*pi*r^3
DELIMITER //

CREATE OR REPLACE FUNCTION ej6(radio float)
RETURNS float
BEGIN
  DECLARE volumen float DEFAULT 0;

  SET volumen=4/3*PI()*POW(radio,3);

  RETURN volumen;
END //
DELIMITER;

SELECT ej6(5);

/* ejercicio 7: Crear un procedimiento almacenado que cree una tabla denominada esferas. Esta tabla tendrá dos campos de tipo
float. Estos campos son radio y volumen. El procedimiento debe crear la tabla aunque esta exista */
DELIMITER //
DROP PROCEDURE IF EXISTS ej7;

CREATE PROCEDURE ej7(IN radio float)
BEGIN
DROP TABLE IF EXISTS esferas;
    CREATE TABLE esferas(
    id int AUTO_INCREMENT PRIMARY KEY,
    radio float,
    volumen float
    );

    INSERT INTO esferas VALUES
    (DEFAULT,radio,ej6(radio));

    SELECT radio,volumen FROM esferas;
END //
DELIMITER;

CALL ej7(2);
CALL ej7(5);

/* ejercicio 8: Crear un procedimiento almacenado que me permita introducir 100 radios de forma automática en la tabla anterior.
Estos radios deben estar entre dos valores pasados al procedimiento como argumentos de entrada */
DELIMITER //
DROP PROCEDURE IF EXISTS ej8;

CREATE PROCEDURE ej8(IN a1 int,IN a2 int)
BEGIN
    DECLARE temp int DEFAULT a1-1;
    
    DELETE FROM esferas;

    REPEAT
       SET temp=temp+1;
       INSERT INTO esferas(radio) VALUES(temp);
    UNTIL (temp>=a2)
    END REPEAT;

    SELECT radio FROM esferas;
END //
DELIMITER;

CALL ej8(2,102);
CALL ej8(205,307);

/* ejercicio 9: Crear un procedimiento almacenado que me permita calcular los volúmenes de todos los registros de la tabla esferas
creada anteriormente. Debéis utilizar la función creada para este propósito */
DELIMITER //
DROP PROCEDURE IF EXISTS ej9;

CREATE PROCEDURE ej9()
BEGIN
  UPDATE esferas SET esferas.volumen=ej6(radio);

END //
DELIMITER;

SELECT radio,volumen FROM esferas;

/* ejercicio 10: - Crear un procedimiento que reciba una palabra como argumento
y la devuelva en ese mismo argumento escrita al revés */

DELIMITER //
DROP PROCEDURE IF EXISTS ej10;

CREATE PROCEDURE ej10(IN palabra varchar(25))
BEGIN
  SELECT REVERSE(palabra);

END //
DELIMITER;

CALL ej10('electroencefalografista');
