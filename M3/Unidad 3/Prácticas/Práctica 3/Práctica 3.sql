﻿﻿                                                       /* Practica 3 */

DROP DATABASE IF EXISTS practica3apoyo;
CREATE DATABASE practica3apoyo;
USE practica3apoyo;

-- Ejercicio 1.
CREATE OR REPLACE TABLE ZonaUrbana(
  NombreZona int AUTO_INCREMENT,
  Categoría char(1),
  PRIMARY KEY(NombreZona)
  );

CREATE OR REPLACE TABLE BloqueCasas(
  Calle int AUTO_INCREMENT,
  Número int,
  Npisos int,
  NombreZona varchar(20),
  PRIMARY KEY(Calle, Número)
  );

CREATE OR REPLACE TABLE CasaParticular(
  NombreZona int,
  Número int,
  Metros int,
  PRIMARY KEY(NombreZona, Número)
  );

CREATE OR REPLACE TABLE Piso(
  Calle int,
  Número int,
  Planta int,
  Puerta int,
  Metros int,
  PRIMARY KEY(Calle, Número, Planta, Puerta)
  );

CREATE OR REPLACE TABLE Persona(
  DNI char(9),
  Nombre varchar(10),
  Edad int,
  NombreZona int,
  NumCasa int,
  Planta int,
  Puerta int,
  PRIMARY KEY(DNI)
  );

CREATE OR REPLACE TABLE PoseeC(
  DNI char(9),
  Calle int,
  NumBloque int,
  Planta int,
  Puerta int,
  PRIMARY KEY(DNI, Calle, NumBloque, Planta, Puerta)
  );

-- Ejercicio 2. Procedimiento que te dice si un valor se encuentra en la tabla.
INSERT INTO ZonaUrbana(NombreZona) VALUES ('105'),('55'),('619');
SELECT * FROM ZonaUrbana;

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(IN arg1 int)
  BEGIN
    IF arg1 IN (SELECT
          NombreZona
        FROM ZonaUrbana) THEN
      SELECT 'El valor se encuentra en la tabla';
    ELSE
      SELECT 'El valor no se encuentra en la tabla';
    END IF;

  /* SELECT nombrezona, LOCATE(arg1, NombreZona) FROM ZonaUrbana; */  -- solo indica la posicion


  END//
DELIMITER ;

CALL ej2(1);

-- solucion ramon
CREATE OR REPLACE PROCEDURE ejercicio2 (arg int)
BEGIN
  DECLARE v1 int; -- variable para cursor
  -- variable para contar registros e imprimir el numero de registro donde encuentre nuestro valor
  DECLARE r int DEFAULT 1;
  -- variable  cuando encuentre el valor o cuando llegue el final de la tabla a recorrer
  DECLARE llave int DEFAULT 1;
  -- cursor para movvermoe por la tabla
  DECLARE c1 CURSOR FOR
  SELECT zu.NombreZona 
  FROM ZonaUrbana zu;

  -- control excepciones para

  OPEN c1;
  REPEAT
    FETCH c1 INTO v1;
    
    IF (v1=arg) THEN
      SELECT
        CONCAT('Valor encontrado en registro',r) salida
      SET llave=0;
    /* else
        select concat('no encontrado ',r) salida; */
      ELSE
        SELECT
          CONCAT('Valor no encontrado en registro',r) salida
        set llave=1
    END IF;
      SET r = r+1;
    UNTIL (llave <> 1)
      END REPEAT
  CLOSE
  end //
  DELIMITER ;

-- Ejercicio 3. Procedimiento que introduzca los valores en la tabla zona_urbana.

DELIMITER //
CREATE OR REPLACE PROCEDURE ej3(IN nombrezona int, IN categoría char(1))
  BEGIN
  INSERT INTO ZonaUrbana VALUES
  (nombrezona, categoría);
  END //
DELIMITER ;

CALL ej3(555, 4);
SELECT * FROM ZonaUrbana;

-- Ejercicio 4. Funcion cuenta registros de la tabla ZonaUrbana

DELIMITER //
CREATE OR REPLACE FUNCTION f3()
  RETURNS int
  COMMENT 'Esta función cuenta el numero de registros que hay en la tabla ZonaUrbana'
  BEGIN
  DECLARE v1 int;
  set v1= (SELECT COUNT(*) FROM ZonaUrbana);
  RETURN v1;
  END //
  DELIMITER ;

SELECT f3();

-- Ejercicio 5. Procedimiento que emplea f3 e introduce el valor en casaparticular siempre que haya algun registro

DELIMITER //
CREATE OR REPLACE PROCEDURE ej5()
  BEGIN
    ALTER TABLE CasaParticular ADD COLUMN (nRegistros int);

    INSERT INTO CasaParticular(nRegistros) VALUES (f3());
  END //
DELIMITER ;

CALL ej5();
SELECT * FROM CasaParticular cp;

-- Ejercicio 6. Crear vista que muestre todos los campos de zona urbana con los campos numero y metros de casaparticular
CREATE OR REPLACE VIEW ej6 AS
  SELECT
   ZonaUrbana.*,CasaParticular.Número,CasaParticular.Metros 
  FROM
   ZonaUrbana JOIN CasaParticular USING(NombreZona)
  ;

SELECT *  FROM ej6;

-- Ejercicio 7. Procedimiento que introduzca de forma automatica 100 registros en ZonaUrbana (usar RAND() y ROUND(num))

DELIMITER //
CREATE OR REPLACE PROCEDURE p6()
  BEGIN
    DECLARE temp int DEFAULT 0;
    ALTER TABLE ZonaUrbana ADD COLUMN IF NOT EXISTS (ID int);

      WHILE (temp<=101) DO
        SET temp=temp+1;
        INSERT INTO ZonaUrbana(ID) VALUES (ROUND(RAND()*temp));
      END WHILE;

  END //
DELIMITER ;

CALL p6();
SELECT * FROM ZonaUrbana;


/* Ejercicio 8. Procedimiento que introduzca 100 registros en CasaParticular.
                Crear funcion que seleccione un valor del campo NombreZona de la tabla ZonaUrbana */

DELIMITER //
CREATE OR REPLACE FUNCTION f4()
  RETURNS int
  COMMENT 'Esta función selecciona un valor aleatorio del campo NombreZona de la tabla ZonaUrbana'
  BEGIN
    DECLARE nRegistros int;
    DECLARE valor int;
      set nRegistros=(SELECT COUNT(*) FROM ZonaUrbana);
      set valor=(RAND()*nRegistros);
  RETURN valor;
  END //
DELIMITER ;

SELECT f4();


DELIMITER //
CREATE OR REPLACE PROCEDURE p7()
  BEGIN
    DECLARE temp int DEFAULT 0;
    ALTER TABLE ZonaUrbana ADD COLUMN IF NOT EXISTS (ID int);

      WHILE (temp<=101) DO
        SET temp=temp+1;
        INSERT INTO ZonaUrbana(ID) VALUES (f4());
      END WHILE;

  END //
DELIMITER ;

CALL p7();
SELECT * FROM ZonaUrbana;
