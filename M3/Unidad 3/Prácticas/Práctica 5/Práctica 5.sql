DROP DATABASE IF EXISTS m3u3p5;
CREATE DATABASE m3u3p5;
USE m3u3p5;

/* Ejercicio 1. Crear un procedimiento que genere una tabla con los campos
  a. Id: Entero, clave principal.
  b. Nombre: Texto. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej1()
  BEGIN
CREATE OR REPLACE TABLE tablaej1(
  id int PRIMARY KEY,
  nombre varchar(15));
  END //
DELIMITER ;

CALL ej1();

/* Ejercicio 2. Colocar en el procedimiento anterior un control de errores de tal forma que si la tabla ya existe en vez de crearla la vac�a. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2()
  BEGIN
  
  DECLARE CONTINUE HANDLER FOR 1050
  BEGIN
  TRUNCATE TABLE tablaej1;
  END;

  CREATE TEMPORARY TABLE tablaej1(
  id int PRIMARY KEY,
  nombre varchar(15));
  END //
DELIMITER ;

CALL ej2();

/* Ejercicio 3. Crea una funci�n que la pasamos dos argumentos (id, nombre). La funci�n debe introducir el dato en la tabla en caso de que el id no exista.
  Si el Id existe entonces lo que realizar� es actualizarlo con el nombre que pasemos. Realizar el ejercicio sin control de errores. */

DELIMITER //
CREATE OR REPLACE FUNCTION ej3(argid int, argnombre varchar(15))
  RETURNS int 
  BEGIN 
  INSERT INTO tablaej1 VALUES (argid, argnombre)
  ON DUPLICATE KEY UPDATE nombre=VALUES (nombre)
  ;
  RETURN 0;
  END //
DELIMITER ;

SELECT ej3(1,'asd');

/* Ejercicio 4. Realizar el ejercicio anterior pero utilizando el control de errores. (Crear una funci�n nueva). */ 

DELIMITER //
CREATE OR REPLACE FUNCTION ej4(argid int, argnombre varchar(15))
  RETURNS int
  BEGIN
  DECLARE CONTINUE HANDLER FOR 1062
  BEGIN
    UPDATE tablaej1 SET nombre=argnombre WHERE id=argid;
  END;
  
  INSERT INTO tablaej1 VALUES(argid,argnombre);
  RETURN 1;
  END //
DELIMITER ;

SELECT ej4(1,'aaaaaa');
SELECT * FROM tablaej1;


/* Ejercicio 5. Crear un script que a�ada un campo a la tabla denominado fecha (tipo DATE). */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej5()
BEGIN
ALTER TABLE tablaej1 ADD COLUMN Fecha date;
END //
DELIMITER ;

CALL ej5();
SELECT * FROM tablaej1;

/* Ejercicio 6. Crear una funci�n a la cual le pasas una fecha y te devuelve el d�a de la semana en castellano al que pertenece. Por ejemplo si
  le paso 05/05/2015 me indicara que es martes. Probarla con la tabla creada. Utilizar la funci�n CASE */
DELIMITER //
 CREATE OR REPLACE FUNCTION ej6(fechaintr date)
RETURNS varchar(15)
  BEGIN
  DECLARE fechaletra varchar(15);
                CASE 
                    WHEN (DAYOFWEEK(fechaintr)=1) THEN set fechaletra='Lunes';
                    WHEN (DAYOFWEEK(fechaintr)=2) THEN set fechaletra='Martes';
                    WHEN (DAYOFWEEK(fechaintr)=3) THEN set fechaletra='Mi�rcoles';
                    WHEN (DAYOFWEEK(fechaintr)=4) THEN set fechaletra='Jueves';
                    WHEN (DAYOFWEEK(fechaintr)=5) THEN set fechaletra='Viernes';
                    WHEN (DAYOFWEEK(fechaintr)=6) THEN set fechaletra='S�bado';
                    WHEN (DAYOFWEEK(fechaintr)=0) THEN set fechaletra='Domingo';
    ELSE set fechaletra='NO VALIDO';
                  END CASE;
                  
  
  RETURN fechaletra;
  END //
DELIMITER ;
SELECT ej6(fecha) FROM tablaej1;

SELECT * FROM tablaej1;

/* Ejercicio 7. Modificar la funci�n anterior para que cuando el dia de la semana sea de lunes a viernes(incluidos) lance una se�al que me grabe en una nueva
  tabla denominada fechas la fecha y el texto laboral. Si el d�a de la semana es s�bado o domingo me lance otra se�al que me grabe en la tabla fechas la fecha
  y el texto festivo. */
CREATE OR REPLACE TABLE fechas(
  fecha date,
  texto varchar (15)
  );

DELIMITER //
 CREATE OR REPLACE FUNCTION ej7(fechaintr date)
RETURNS varchar(15)
  BEGIN
  DECLARE fechaletra varchar(15);
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01000'
    BEGIN
      INSERT INTO fechas VALUES(fechaintr, 'laboral');
    END;
  DECLARE CONTINUE HANDLER FOR SQLSTATE '01001'
    BEGIN
      INSERT INTO fechas VALUES(fechaintr, 'festivo');
    END;

  CASE DAYOFWEEK(fechaintr)
    WHEN 2 THEN
      SIGNAL SQLSTATE '01000';
      RETURN 'Lunes';
    WHEN 3 THEN
      SIGNAL SQLSTATE '01000';
      RETURN 'Martes';
    WHEN 4 THEN
      SIGNAL SQLSTATE '01000';
      RETURN 'Mi�rcoles';
    WHEN 5 THEN
      SIGNAL SQLSTATE '01000';
      RETURN 'Jueves';
    WHEN 6 THEN
      SIGNAL SQLSTATE '01000';
      RETURN 'Viernes';
    WHEN 7 THEN
      SIGNAL SQLSTATE '01001';
      RETURN 'S�bado';
    WHEN 1 THEN
      SIGNAL SQLSTATE '01001';
      RETURN 'Domingo';
END CASE;
END //

DELIMITER ;



INSERT INTO tablaej1 VALUES (1, 'Pedro', '2019-02-26'), (2, 'Mar�a', '2019-02-17'), (3, 'Jos�', '2019-02-25');
SELECT ej7(fecha) FROM tablaej1;
SELECT * FROM fechas;
SELECT * FROM tablaej1;


/* Ejercicio 9. Modificar el ejercicio del procedimiento almacenado solicitado en el ejercicio 6 para realizarlo una tabla temporal como la que vemos en la imagen. */
DELIMITER //
 CREATE OR REPLACE FUNCTION ej9(fechaintr date)
RETURNS varchar(15)
  BEGIN
    DECLARE fechaletra varchar(15);

  DROP TEMPORARY TABLE IF EXISTS temporal;
  CREATE TEMPORARY TABLE temporal(
    id int,
    nombre varchar(15)
    );

  INSERT INTO temporal VALUES (1, 'Domingo'),
    (2, 'Lunes'),
    (3, 'Martes'),
    (4, 'Mi�rcoles'),
    (5, 'Jueves'),
    (6, 'Viernes'),
    (7, 'S�bado');

  SET fechaletra=(SELECT nombre FROM temporal WHERE id=DAYOFWEEK(fechaintr));                 
  
  RETURN fechaletra;
  END //
DELIMITER ;

SELECT ej9(fecha) FROM tablaej1;