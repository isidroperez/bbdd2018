﻿-- Ejercicios 1 y 2 (area y volumen)
DROP DATABASE IF EXISTS practica1apoyo;
CREATE DATABASE practica1apoyo;
USE practica1apoyo;

DELIMITER //
DROP PROCEDURE IF EXISTS practica1apoyo.ejercicio1 //
 CREATE PROCEDURE practica1apoyo.ejercicio1(IN lado float)
  BEGIN
  DECLARE area float;
  DECLARE superficie float;
  DECLARE volumen float;
  set AREA = POW(lado, 2);
  set superficie = lado*4;
  set volumen = POW(lado,3);
  CREATE OR REPLACE table ejercicio1(
  numero int AUTO_INCREMENT,
  lado float,
  area float,
  superficie float,
  volumen float,
  PRIMARY KEY(numero)
  );
  INSERT INTO ejercicio1 VALUES
  (DEFAULT, lado, area, superficie, volumen);
  END //

DELIMITER ;

CALL ejercicio1(5);
SELECT * FROM  ejercicio1;


-- Ejercicio 3. Area y volumen del cubo.
DELIMITER //
DROP PROCEDURE IF EXISTS practica1apoyo.ejercicio3 //
 CREATE PROCEDURE practica1apoyo.ejercicio3(IN lado float)
  BEGIN
  DECLARE area float;
  DECLARE volumen float;
  set area = 6*(lado*lado);
  set volumen = POW(lado,3);
  CREATE OR REPLACE table ejercicio3(
  numero int AUTO_INCREMENT,
  lado float,
  area float,
  volumen float,
  PRIMARY KEY(numero)
  );
  INSERT INTO ejercicio3 VALUES
  (DEFAULT, lado, area, volumen);
  END //

DELIMITER ;

CALL ejercicio3(6);
SELECT * FROM ejercicio3;

-- Ejercicio 4. Función para calcular el volumen
DELIMITER //
CREATE OR REPLACE FUNCTION practica1apoyo.ejercicio1 (lado int)
  RETURNS int
  BEGIN
  DECLARE volumen float;
  set volumen=POW(lado,3);
  RETURN volumen;

  END//
DELIMITER ;
SELECT ejercicio1(5);


-- Ejercicio 5. Función para área del cubo.
  DELIMITER //
CREATE OR REPLACE FUNCTION practica1apoyo.ejercicio2 (lado int)
  RETURNS int
  BEGIN
  DECLARE area float;
  set area=6*(lado*lado);
  RETURN area;

  END//
DELIMITER ;
SELECT ejercicio2(4);


-- Ejercicio 6. Procedimiento almacenado para las funciones anteriores.

DELIMITER //
CREATE OR REPLACE PROCEDURE ejercicio6(IN lado float)
  BEGIN
  DECLARE volumen float;
  DECLARE areatotal float;

  set volumen=ejercicio1(lado);
  set areatotal=ejercicio2(lado);

  CREATE OR REPLACE TABLE ejercicio6(
  id int AUTO_INCREMENT,
  lado float,
  volumen float,
  area float,
  PRIMARY KEY(id)
  );

  INSERT INTO ejercicio6 VALUES(DEFAULT, lado, volumen, areatotal);
  END //
DELIMITER ;

CALL ejercicio6(8);
SELECT * FROM ejercicio6;

-- Ejercicio 7. Volumen del cilindro, función.

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio7(radio float, altura float)
  RETURNS float
  BEGIN
  DECLARE volumen float;
  SET volumen=PI() * POW(radio,2) * altura;
  RETURN volumen;
  END //
DELIMITER ;

SELECT ejercicio7(5,3);


-- Ejercicio 8. Área lateral, función.

DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio8(radio float, altura float)
  RETURNS float
  BEGIN
  DECLARE arealateral float;
  set arealateral=2*PI()*radio*altura;
  RETURN arealateral;
  END //
DELIMITER ;
SELECT ejercicio8(5,4);

-- Ejercicio 9. Calcular ára total, función.
DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio9(radio float, altura float)
  RETURNS float
  BEGIN
  DECLARE areatotal float;
  set areatotal=(2*PI()*radio*altura)+(2*PI()*POW(radio,2));
  RETURN areatotal;
  END //
DELIMITER ;

SELECT ejercicio9 (9,15);

-- Ejercicio 10. Procedimiento que utilice las funciones anteriores.
DELIMITER //
CREATE OR REPLACE PROCEDURE ejercicio10(IN radio float, IN altura float)
  BEGIN
  DECLARE volumen float;
  DECLARE arealateral float;
  DECLARE areatotal float;
  set volumen = ejercicio7(radio, altura);
  SET arealateral = ejercicio8(radio, altura);
  SET areatotal = ejercicio9(radio, altura);
CREATE OR REPLACE TABLE ejercicio10(
  id int AUTO_INCREMENT,
  radio float,
  altura float,
  volumen float,
  arealateral float,
  areatotal float,
  PRIMARY KEY(id)
  );
  INSERT INTO ejercicio10 VALUES (DEFAULT, radio, altura, volumen, arealateral, areatotal);
  END //
DELIMITER ;

CALL ejercicio10(7,9);
SELECT * FROM ejercicio10;
