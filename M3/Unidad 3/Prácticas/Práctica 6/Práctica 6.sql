﻿DROP DATABASE IF EXISTS m3u3p6;
CREATE DATABASE m3u3p6;
USE m3u3p6;


/* Ejercicio 1. Convertir en el siguiente procedimiento almacenado el LOOP en un WHILE. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej1()
  BEGIN
  DECLARE counter bigint DEFAULT 0;

  WHILE counter <10 DO
  SET counter=counter+1;

  SELECT counter;
  END WHILE;
  END //
DELIMITER;

CALL ej1();

/* Ejercicio 2. Realizar el ejercicio anterior con un REPEAT. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej2()
  BEGIN
  DECLARE counter bigint DEFAULT 0;

  REPEAT
    SET counter=counter+1;
    SELECT counter;
  UNTIL
    counter = 10
  END REPEAT;
  END //
DELIMITER;

CALL ej2();



/* Ejercicio 4. Analizar el siguiente procedimiento. Implementarlo y probarlo. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4()
  BEGIN
  DECLARE v_name varchar(120);
  DECLARE v_time bigint;
  DECLARE v_penalty1 bigint;
  DECLARE v_penalty2 bigint;

  DECLARE fin integer DEFAULT 0;

  DECLARE runners_cursor CURSOR FOR
    SELECT Name, time, Penalty1, Penalty2 FROM runners;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;

  OPEN runners_cursor;
  GET_runners: LOOP
  FETCH runners_cursor INTO v_name, v_time, v_penalty1, v_penalty2;
  IF fin=1 THEN
  LEAVE get_runners;
  END IF;

  SELECT v_name, v_time, v_penalty1, v_penalty2;

  END LOOP GET_runners;
  CLOSE runners_cursor;
  END //
DELIMITER;

CALL ej4();


/* Ejercicio 5. Modificar el procedimiento anterior para que puedas pasarle un argumento que sería el número de registros a mostrar. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(nregistros int)
  BEGIN
  DECLARE v_name varchar(120);
  DECLARE v_time bigint;
  DECLARE v_penalty1 bigint;
  DECLARE v_penalty2 bigint;
  DECLARE contador int DEFAULT 0;
  DECLARE fin integer DEFAULT 0;

  DECLARE runners_cursor CURSOR FOR
    SELECT Name, time, Penalty1, Penalty2 FROM runners;

  DECLARE CONTINUE HANDLER FOR NOT FOUND SET fin=1;

  OPEN runners_cursor;
  GET_runners: LOOP
  FETCH runners_cursor INTO v_name, v_time, v_penalty1, v_penalty2;
  IF (fin=1 OR contador=nregistros) THEN
  LEAVE get_runners;
  END IF;

  SELECT v_name, v_time, v_penalty1, v_penalty2;
  set contador=contador+1;

  END LOOP GET_runners;
  CLOSE runners_cursor;
  END //
DELIMITER;

CALL ej5(4);

/* Ejercicio 6. Vamos a crear una función que asigne las puntuaciones a cada uno de los corredores con una fórmula. Por ejemplo la siguiente: siendo Time el tiempo en
  segundos que se tarda en realizar la prueba, 500-Time serán los puntos iniciales, a los que tenemos que restar 5*penalty1 y 3*penalty2. A la función la llamaré de la
  siguiente forma

  UPDATE Runners SET Points=calculate_runner_points(v_time, v_penalty1, v_penalty2); */

DELIMITER //
CREATE OR REPLACE FUNCTION calculate_runner_points(tiempo int, penalty1 int, penalty2 int)
  RETURNS int
  BEGIN
  DECLARE puntos int;
  set puntos=(500-tiempo)-(penalty1*5)-(penalty2*3);
  RETURN puntos;
  END //
DELIMITER ;

UPDATE Runners SET Points=calculate_runner_points(time, penalty1, penalty2);

SELECT * FROM runners;