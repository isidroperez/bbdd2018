﻿/* Practica 2 */

DROP DATABASE IF EXISTS practica2apoyo;
CREATE DATABASE practica2apoyo;
USE practica2apoyo;

-- Ejercicio 1: Explicar que realiza el siguiente procedimiento y ejecutarlo
DELIMITER //

DROP PROCEDURE IF EXISTS f1;
CREATE PROCEDURE f1()
COMMENT 'Procedimiento almacenado que crea una tabla donde almacena el nombre, la edad y la fecha de nacimiento.

         NO NECESITA de ningun tipo de argumento.         
         Como funciona:
         CALL f1();
         SELECT * FROM tabla1;' 
BEGIN
-- creamos la tabla1 con los siguientes campos:
CREATE TABLE tabla1(
nombre varchar(10) DEFAULT NULL,     -- campo nombre de tipo texto con un limite de 10 caracteres que tendra por defecto NULL
edad int(3) DEFAULT 0,               -- campo edad de tipo numero entero de hasta 3 digitos que tendra por defecto 0
Fecha_nacimiento date DEFAULT NULL  -- campo fecha de nacimiento tipo fecha que tendra por defecto NULL
);

END//
DELIMITER ;

CALL f1();
SELECT * FROM tabla1;


-- Ejercicio 2: Modificar el procedimiento anterior para crear una tabla con los siguientes campos:
DELIMITER //

DROP PROCEDURE IF EXISTS crear_tabla;
CREATE PROCEDURE crear_tabla()
  COMMENT 'Procedimiento almacenado que crea una tabla donde se almacena los datos de las personas

         NO NECESITA de ningun tipo de argumento.         
         Como funciona:
         CALL crear_tabla();
         SELECT * FROM personas;'
BEGIN

CREATE TABLE personas(
dni float(8) PRIMARY KEY,
nombre varchar(30),
edad int,
apellidos varchar(50),
fecha_nacimiento date
);

END//
DELIMITER ;

CALL crear_tabla();
SELECT * FROM personas;


-- Ejercicio 3: Explicar que realiza el siguiente procedimiento y ejecutarlo
DELIMITER //

DROP PROCEDURE IF EXISTS f2;
CREATE PROCEDURE f2()
COMMENT 'Procedimiento almacenado que escribe un mensaje indicando si la tabla esta o no vacia de registros

         NO NECESITA de ningun tipo de argumento.         
         Como funciona:
         CALL f2();' 
BEGIN
  DECLARE v1 int;
  
  SET v1=(SELECT COUNT(*) FROM tabla1);        -- cuenta los registros de la tabla

  IF (v1=0) THEN                                -- compara si hay o no registros en la tabla
      SELECT 'la tabla esta vacia' salida;     -- indica en pantalla, con un mensaje aclaratorio, si la tabla no tiene ningun registros
  ELSE
      SELECT 'la tabla no esta vacia' salida;  -- indica en pantalla, con un mensaje aclaratorio, si la tabla tiene algun registros
  END IF;
  
      
END//
DELIMITER ;

CALL f2();
-- (n=0, (SELECT * FROM tabla1), IF(n=1, (SELECT * FROM personas), 'valor incorrecto'));

-- Ejercicio 4. Modificar el proocedimiento para que si le pasamos un 0 te evalua la tabla denominada tabla1, si es un 1 evalúa la tabla denominada personas.
 -- Si se le indica otro valor, incorrecto.

DELIMITER //
CREATE OR REPLACE PROCEDURE ej4(IN n int)
  COMMENT 'Este procedimiento devolverá una tabla u otra dependiendo del valor de entrada introducido'
  BEGIN
  IF n=0 THEN (SELECT * FROM tabla1);
   ELSE IF n=1 THEN (SELECT * FROM personas);
  ELSE SELECT 'valor incorrecto';
   END IF;
  END IF;
  END //
DELIMITER ;

-- Ejercicio 5. Explicar qué hace el procedimiento.
DELIMITER //
CREATE OR REPLACE PROCEDURE ejercicio5()
  BEGIN
  DECLARE v1 int;
  SET v1=(SELECT COUNT(*) FROM tabla1); -- Declara la variable como la cuenta de registros de la tabla 1.
  IF (v1=0) THEN
  SELECT 'la tabla está vacía' salida; -- Si no hay registros, devuelve 'la tabla está vacía' e introduce un registro con valores por defecto.
  INSERT INTO TABLA1 VALUES(DEFAULT, DEFAULT, DEFAULT);
  ELSE IF (v1 BETWEEN 10 AND 100) THEN SELECT * FROM tabla1; -- en caso de que esté entre 10 y 100, muestra toda la tabla.
  ELSE SELECT * FROM tabla1 LIMIT 1,100; -- Si ninguno de los 2 casos es correcto, devuelve los 100 primeros valores.
  END IF;
  END IF;
  END //
  DELIMITER ;

-- Ejercicio 6. Convertirlo en función.
DELIMITER //
CREATE OR REPLACE FUNCTION ejercicio6()
  RETURNS int
  COMMENT 'Esta función introducirá un registro en la tabla1 en caso de que v1 (cuenta de registros) sea 0'
  BEGIN
  DECLARE v1 int;
  set v1= (SELECT COUNT(*) FROM tabla1);
   IF (v1=0) THEN
  INSERT INTO TABLA1 VALUES(DEFAULT, DEFAULT, DEFAULT);
  END IF;
  RETURN v1;
  END //
  DELIMITER ;
SELECT ejercicio6();

/* Ejercicio 7. Realizar un procedimiento, denominado crear_claves, que genera las siguientes claves y restricciones en la tabla personas:
  a. Clave principal: dni
  b. Clave única: nombre y apellidos
  c. Indexado con duplicados: fecha_nacimiento */

DELIMITER //
CREATE OR REPLACE PROCEDURE crear_claves()
  COMMENT 'Este procedimiento crea los constraints clave principal en dni,
           clave única en nombre y apellidos y el campo fecha nacimiento lo mete como indexado con duplicados'
  BEGIN
  ALTER TABLE personas ADD CONSTRAINT PRIMARYKEYDNI PRIMARY KEY(dni);
  ALTER TABLE personas ADD CONSTRAINT uniquekeynombre UNIQUE KEY(nombre,apellidos);
  ALTER TABLE personas ADD INDEX (fecha_nacimiento);
  END //
DELIMITER ;

-- Ejercicio 8. Crear un procedimiento que actualice todos los campos edad calculándolos en función de la fecha de nacimiento.
DELIMITER //
CREATE OR REPLACE PROCEDURE actualizar_edad()
  BEGIN
  UPDATE personas
  set edad=TIMESTAMPDIFF(year, NOW(),fecha_nacimiento);
  END //
DELIMITER ;
                                    
CALL actualizar_edad();

-- Ejercicio 9. Crear una vista que nos permita sacar todas las personas menores de 18 años.
CREATE OR REPLACE VIEW ej9 AS
  SELECT nombre, apellidos, edad FROM personas
  WHERE edad <18;
 
-- Creando una tabla para los menores de 18.
CREATE OR REPLACE TABLE menores SELECT * FROM ej9;

-- Creando una tabla para el resto de registros (mayores de 18 años)
CREATE OR REPLACE TABLE no_menores SELECT * FROM personas lEFT JOIN ej9 USING(nombre,apellidos) WHERE ej9.nombre IS NULL;




-- Ejercicio 10. Crear una vista que contenga los nombres y apellidos y meterlos en una tabla
  CREATE OR REPLACE VIEW ej10 AS
    SELECT CONCAT(apellidos, nombre) nombre FROM personas;

  CREATE OR REPLACE TABLE nombre_completo SELECT nombre FROM ej10;

-- Ejercicio 11. Procedimiento el cual te rellene una tabla denominada alfabeto con todas las letras de la a a la z.
  -- Crear otro procedimiento que te rellene una tabla denominado números con todos los números del 1 al 1000.
DELIMITER //
CREATE OR REPLACE PROCEDURE crear_alfabeto()
  BEGIN
  DECLARE temp int DEFAULT 96; -- variable contador
  CREATE OR REPLACE TABLE alfabeto(
  ID int PRIMARY KEY,
  letra char(1));

  -- Creando bucle.
  WHILE (temp<=121) DO

  SET temp=temp+1; -- actualizando contador
  INSERT INTO alfabeto VALUES(temp, char(temp));
  END WHILE;

  -- Fin del bucle etiqueta.
  END //
DELIMITER ;


CALL crear_alfabeto();
SELECT * FROM alfabeto;


DELIMITER //
CREATE OR REPLACE PROCEDURE crear_numero()
  BEGIN
  DECLARE temp int DEFAULT 0; -- variable contador
  CREATE OR REPLACE TABLE numero(
  numero int);

  -- Creando bucle.
  WHILE (temp<=1001) DO

  SET temp=temp+1; -- actualizando contador
  INSERT INTO numero VALUES(temp);
  END WHILE;

  -- Fin del bucle etiqueta.
  END //
DELIMITER ;


CALL crear_numero();
SELECT * FROM numero;