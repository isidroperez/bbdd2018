﻿
/* Ejercicio 2. Función área triángulo.
  argumentos Base y altura. */
DELIMITER //
CREATE OR REPLACE FUNCTION AreaTriangulo(base float, altura float)
  RETURNS float
  COMMENT 'Esta función nos devolverá el área del triángulo (base por altura partido de 2)'
  BEGIN
  DECLARE areatriangulo float DEFAULT 0;
  SET areatriangulo=(base*altura)/2;
  RETURN areatriangulo;

  END //
DELIMITER ;

SELECT AreaTriangulo(12,6);

/* Ejercicio 3. Función perímetro triángulo
  Argumentos: base, lado1, lado 2 */

DELIMITER //
CREATE OR REPLACE FUNCTION PerimetroTriangulo(base float, lado1 float, lado2 float)
  RETURNS float
  COMMENT 'Esta función nos devolverá el perímetro del triángulo (la suma de sus lados)'
  BEGIN
  DECLARE perimetrotr float DEFAULT 0;
  SET perimetrotr=base+lado1+lado2;
  RETURN perimetrotr;

END //
DELIMITER ;
SELECT PerimetroTriangulo(9,5,6);

/* Ejercicio 4. Procedimiento almacenado que actualice el área y el perímetro de los triángulos
  argumentos ID1, ID2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ActualizarTriangulos(IN ID1 int,IN ID2 int)
  COMMENT 'Este procedimiento actualizará entre los valores introducidos el área y el perímetro de los triángulos'
  BEGIN 
  UPDATE triangulos -- Primero los vaciamos
  SET area=NULL;
  UPDATE triangulos
  set perimetro=NULL;
  UPDATE triangulos -- Despues nos quedamos con los valores que queramos
  SET area=AreaTriangulo(base, altura) WHERE id BETWEEN ID1 AND ID2;
  UPDATE triangulos
  set perimetro=PerimetroTriangulo(base,lado2,lado3) WHERE id BETWEEN ID1 AND id2;
  END//
DELIMITER ;

CALL ActualizarTriangulos(9,15);

SELECT * FROM  triangulos;


/* Ejercicio 5. Función del área del cuadrado
  Argumentos: Lado. */
DELIMITER //
CREATE OR REPLACE FUNCTION AreaCuadrado(lado float)
  RETURNS float
  COMMENT 'Función para calcular el área de un cuadrado (lado por lado)'
  BEGIN
  DECLARE areacuadrado float;
  set areacuadrado=lado*lado;
  RETURN areacuadrado;
  END //
DELIMITER ;

SELECT AreaCuadrado(6);

/* Ejercicio 6. Función perímetro del cuadrado
  Argumentos: Lado. */
DELIMITER //
CREATE OR REPLACE FUNCTION PerimetroCuadrado(lado float)
  RETURNS float
  COMMENT 'Función para calcular el perímetro de un cuadrado (la suma de sus lados)'
  BEGIN
  DECLARE perimetrocuadrado float;
  set perimetrocuadrado=lado*4;
  RETURN perimetrocuadrado;
  END //
DELIMITER ;

SELECT PerimetroCuadrado(5);



/* Ejercicio 7. Procedimiento almacenado que actualice el área y el perímetro de los cuadrados
  argumentos ID1, ID2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ActualizarCuadrados(IN ID1 int, IN ID2 int)
COMMENT 'Este procedimiento actualizará entre los valores introducidos el área y el perímetro de los cuadrados'
  BEGIN
  UPDATE cuadrados -- Primero vaciamos los valores.
  SET area=NULL;
  UPDATE cuadrados 
  SET perimetro=NULL;
  UPDATE cuadrados -- Aqui metemos los que queramos.
  SET area=AreaCuadrado(lado) WHERE id BETWEEN ID1 AND id2;
  UPDATE cuadrados
  SET perimetro=PerimetroCuadrado(lado) WHERE id BETWEEN ID1 AND id2;
  END //
DELIMITER ;

CALL ActualizarCuadrados(8,10);

SELECT * FROM cuadrados;


/* Ejercicio 8. Función del área del círculo.
  Argumentos Radio */
DELIMITER //
CREATE OR REPLACE FUNCTION AreaCirculo(radio float)
  RETURNS float
  COMMENT 'Esta función nos devolverá el área del círculo introduciendo su radio (pi por el radio al cuadrado)'
  BEGIN
  DECLARE areacirculo float;
  SET areacirculo=(PI()*(radio*radio));
  RETURN areacirculo;
  END //
DELIMITER ;

SELECT AreaCirculo(6);

/* Ejercicio 9. Función perímetro de círculo.
  Argumentos Radio */
DELIMITER //
CREATE OR REPLACE FUNCTION PerimetroCirculo(radio float)
  RETURNS float
  COMMENT 'Esta función nos devolverá el perímetro del círculo introduciendo su radio (2 por pi por el radio)'
  BEGIN
  DECLARE perimetrocirculo float;
  set perimetrocirculo=2*PI()*radio;
  RETURN perimetrocirculo;
  END //
DELIMITER ;

SELECT PerimetroCirculo(7);

/* Ejercicio 10. Procedimiento almacenado que actualice el área y el perímetro de los círculos
  argumentos ID1, ID2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ActualizarCirculos(IN ID1 int, IN ID2 int)
  COMMENT 'Este procedimiento actualizará desde el ID de los valores introducidos (de uno al otro) el área y el perímetro de los círculos'
  BEGIN
  UPDATE circulo -- Aqui vaciamos los valores.
  SET area=NULL;
  UPDATE circulo
  SET perimetro=NULL;
  UPDATE circulo -- Aqui introducimos los valores que queramos.
  SET area=AreaCirculo(radio) WHERE id BETWEEN ID1 and ID2;
  UPDATE circulo
  set perimetro=PerimetroCirculo(radio) WHERE id BETWEEN ID1 and ID2;
  END //
DELIMITER ;


CALL ActualizarCirculos(2,6);
SELECT * FROM circulo;


/* Función para calcular el área del rectángulo
  Argumentos lado1, lado2 */
DELIMITER //
CREATE OR REPLACE FUNCTION AreaRectangulo(lado1 float, lado2 float)
RETURNS float
COMMENT 'Esta función devuelve el área del rectángulo (base por altura) a partir de lado1 y lado2'
  BEGIN
  DECLARE arearectangulo float;
  set arearectangulo=lado1*lado2;
  RETURN arearectangulo;
END //
DELIMITER ;

SELECT AreaRectangulo(9,5);


/* Función para calcular el perímetro del rectángulo
  Argumentos lado1, lado2 */
DELIMITER //
CREATE OR REPLACE FUNCTION PerimetroRectangulo(lado1 float, lado2 float)
  RETURNS float
  COMMENT 'Esta función devuelve el perímetro del rectángulo (base por 2 + altura por 2) a partir de lado 1 y lado 2'
  BEGIN
  DECLARE perimetrocuadrado float;
  set perimetrocuadrado=(lado1*2)+(lado2*2);
  RETURN perimetrocuadrado;
END //
DELIMITER ;

SELECT PerimetroRectangulo(6,4);

/* Procedimiento almacenado que actualice el área y el perímetro de los rectángulos
  argumentos ID1, ID2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ActualizarRectangulos(IN ID1 int, IN ID2 int)
  COMMENT 'Este procedimiento actualizará desde el ID de los valores introducidos (de uno al otro) el área y el perímetro de los rectángulos'
  BEGIN
  UPDATE rectangulo
  SET area=NULL;
  UPDATE rectangulo
  SET perimetro=NULL;
  UPDATE rectangulo
  SET area=AreaRectangulo(lado1, lado2) WHERE id BETWEEN id1 AND id2;
  UPDATE rectangulo
  SET perimetro=PerimetroRectangulo(lado1, lado2) WHERE id BETWEEN id1 AND id2;
  END //
DELIMITER ;

CALL ActualizarRectangulos(4,9);

SELECT * FROM rectangulo;

/* Ejercicio 11. Función para calcular la media de las 4 notas
    Argumentos nota1, nota2, nota3 y nota4 */
DELIMITER //
CREATE OR REPLACE FUNCTION Media(nota1 float, nota2 float, nota3 float, nota4 float)
RETURNS float
COMMENT 'Esta función te devuelve la media entre 4 valores indtroducidos'
begin
DECLARE media float;
SET media=(nota1+nota2+nota3+nota4)/4;
RETURN media;
END //
DELIMITER ;

/* Ejercicio 12. Función para calcular el mínimo de las 4 notas
    Argumentos nota1, nota2, nota3 y nota4 */
DELIMITER //
  CREATE OR REPLACE FUNCTION Minimo(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
  COMMENT 'Esta función te devuelve el minimo entre 4 valores indtroducidos'
  begin
  DECLARE minimo float;
  SET minimo=LEAST(nota1,nota2,nota3,nota4);
  RETURN minimo;
  END //
DELIMITER ;

/* Ejercicio 13. Función para calcular el máximo de las 4 notas
    Argumentos nota1, nota2, nota3 y nota4 */
DELIMITER //
  CREATE OR REPLACE FUNCTION Maximo(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
  COMMENT 'Esta función te devuelve el máximo entre 4 valores indtroducidos'
  begin
  DECLARE maximo float;
  SET maximo=GREATEST(nota1,nota2,nota3,nota4);
  RETURN maximo;
  END //
DELIMITER ;

SELECT maximo(5,1,9,8);

/* Ejercicio 14. Función para calcular la moda de las 4 notas.
    Argumentos nota1, nota2, nota3 y nota 4 */
DELIMITER //
CREATE OR REPLACE FUNCTION Moda(nota1 float, nota2 float, nota3 float, nota4 float)
  RETURNS float
  COMMENT 'Esta función devolverá la nota más repetida de todas'
  BEGIN
  DECLARE moda float;

  CREATE OR replace TEMPORARY TABLE tablamodas(
    id int AUTO_INCREMENT,
    notas float,
    PRIMARY KEY(id)
    );

  INSERT INTO tablamodas(notas) VALUES(nota1),(nota2),(nota3),(nota4);
  
  SELECT notas INTO moda FROM tablamodas GROUP BY notas ORDER BY COUNT(*) DESC LIMIT 1;
  RETURN moda;
  END //
DELIMITER ;
 


/* Ejercicio 15. Procedimiento almacenado que actualice los campos de la tabla alumnos
    argumentos ID1, ID2 */
DELIMITER //
CREATE OR REPLACE PROCEDURE ActualizarAlumnos(IN ID1 int, IN ID2 int)
  COMMENT 'Este procedimiento actualizará desde el ID de los valores introducidos la media, mínimo, máximo y moda de las notas.'
  BEGIN
  UPDATE alumnos
  SET media=NULL;
  UPDATE alumnos
  SET min=NULL;
  UPDATE alumnos
  SET max=NULL;
  UPDATE alumnos
  SET moda=NULL;
  UPDATE alumnos
  SET media=Media(nota1, nota2, nota3, nota4) WHERE id BETWEEN ID1 AND ID2;
  UPDATE alumnos
  SET min=Minimo(nota1, nota2, nota3, nota4) WHERE id BETWEEN ID1 AND ID2;
  UPDATE alumnos
  SET max=Maximo(nota1, nota2, nota3, nota4) WHERE id BETWEEN ID1 AND ID2;
  UPDATE alumnos
  SET moda=Moda(nota1, nota2, nota3, nota4) WHERE id BETWEEN ID1 AND ID2;
  UPDATE grupos
    SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=1)
      WHERE id=1;
  UPDATE grupos   
     SET media=(SELECT AVG(media) FROM alumnos WHERE grupo=2)
      WHERE id=2;
  END //
DELIMITER ;

CALL ActualizarAlumnos(1,6);
SELECT * FROM alumnos;

/* Procedimiento almacenado de conversión. */

DELIMITER //
CREATE OR REPLACE PROCEDURE actualizarConversion(
  IN id int)
BEGIN
  UPDATE conversion
    set
    m=cm/100,
    km=cm/100000,
    pulgadas=cm/0.393701
  where cm is not NULL;
  END //
DELIMITER ;