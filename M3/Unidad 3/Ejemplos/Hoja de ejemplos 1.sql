﻿     /* Ejemplo 1 */

/* 1. Realizar un procedimiento almacenado que reciba DOS numeros y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una funcion de Mysql */

 -- a) con instruccion if
 
  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1a //

  CREATE PROCEDURE ejem1_1a(num1 int, num2 int)
  BEGIN
    IF (num1>num2) THEN SELECT num1;
      ELSE SELECT num2;
    END IF;
  END //
    DELIMITER;

CALL ejem1_1a(3,4);
CALL ejem1_1a(7,6);

-- b) con consulta de totales

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1b //

  CREATE PROCEDURE ejem1_1b(num1 int, num2 int)
  BEGIN
    DROP TABLE IF EXISTS tabla1_1b;
    CREATE TEMPORARY TABLE tabla1_1b(
    id int AUTO_INCREMENT PRIMARY KEY,
    numero1 int
    );
    INSERT INTO tabla1_1b VALUES
    (DEFAULT,num1),
    (DEFAULT, num2);
    SELECT MAX(numero1) FROM tabla1_1b;
  END //
  DELIMITER;

CALL ejem1_1b(3,4);
CALL ejem1_1b(7,6);

-- c) con funcion MySQL

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_1c //

  CREATE PROCEDURE ejem1_1c(num1 int, num2 int)
  BEGIN
    SELECT GREATEST(num1,num2);
  END //
  DELIMITER;

CALL ejem1_1c(3,4);
CALL ejem1_1c(7,6);


/* 2. Realizar un procedimiento almacenado que reciba TRES numeros y te indique el mayor de ellos (realizarle con instruccion if,
  con consulta de totales y con una funcion de Mysql */

 -- a) con instruccion if
 
  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_2a //

  CREATE PROCEDURE ejem1_2a(num1 int, num2 int, num3 int)
  BEGIN
    IF (num1>num2) THEN SELECT num1;
      ELSE SELECT num2;
    END IF;
  END //
    DELIMITER;

CALL ejem1_2a(3,4,8);
CALL ejem1_2a(7,6,2);

-- b) con consulta de totales

  DELIMITER //
  DROP PROCEDURE IF EXISTS ej2v2 //

  CREATE PROCEDURE ej2v2(num1 int, num2 int, num3 int)
  BEGIN
    DROP TABLE IF EXISTS tabla_2b;
    CREATE TEMPORARY TABLE tabla_2b(
    id int AUTO_INCREMENT PRIMARY KEY,
    numero1 int
    );
    INSERT INTO tabla_2b VALUES
    (DEFAULT,num1),
    (DEFAULT,num2),
    (DEFAULT,num3);

    SELECT MAX(numero1) FROM tabla_2b;
  END //
  DELIMITER;

CALL ej2v2(3,4,8);
CALL ej2v2(7,6,2);

-- c) con funcion MySQL

  DELIMITER //
  DROP PROCEDURE IF EXISTS ejem1_2c //

  CREATE PROCEDURE ejem1_2c(num1 int, num2 int,num3 int)
  BEGIN
    SELECT IF(num1>num2,num1,IF(num2>num3,num2,num3));
  END //
  DELIMITER;

CALL ejem1_2c(3,4,8);
CALL ejem1_2c(7,6,2);
