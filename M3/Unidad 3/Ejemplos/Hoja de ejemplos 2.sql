﻿USE m3u3ej2;

/* EJERCICIO 1. Realizar un procedimiento almacenado que reciba un texto y un carácter. Debe indicarte si ese carácter está en el texto.
  Debéis realizarlo con: Locate, Position  */
DROP PROCEDURE IF EXISTS ej1v1;
DELIMITER //
CREATE PROCEDURE ej1v1(texto varchar(25), caracter char)
  BEGIN
IF (SELECT LOCATE(caracter, texto)) 
  THEN 
  SELECT 'Verdadero';
ELSE 
  SELECT 'Falso';
END IF;
  END //
  DELIMITER ;

CALL ej1v1 ('casa', 'asa');
CALL ej1v1 ('casa', 'o');


DROP PROCEDURE IF EXISTS ej1v2;
DELIMITER //
CREATE PROCEDURE ej1v2(texto varchar(25), caracter char)
  BEGIN
IF (SELECT POSITION(caracter IN texto)) 
  THEN 
  SELECT 'Verdadero';
ELSE 
  SELECT 'Falso';
END IF;
  END //
  DELIMITER ;

CALL ej1v2 ('casa', 'asa');
CALL ej1v2 ('casa', 'o');


/* EJERCICIO 2. Realizar un procedimiento almacenado que reciba un texto y un carácter. Te debe indicar 
  toodo el texto que haya antes de la primera vez que aparece ese carácter. */

DROP PROCEDURE IF EXISTS ej2v1;
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2v1(texto varchar(25), caracter char)
  BEGIN
SELECT SUBSTRING_INDEX(texto, caracter, 1);
  END //
  DELIMITER ;

CALL ej2v1 ('esternocleidomastoideo', 'i');

/* EJERCICIO 3. Realizar un procedimiento almacenado que reciba tres números y dos argumentos de tipo salida donde devuelva
  el número más grande y el número más pequeño de los tres números pasados. */

DROP PROCEDURE IF EXISTS ej3v1;
DELIMITER //
CREATE OR REPLACE PROCEDURE ej3v1(num1 int, num2 int, num3 int, OUT arg1 int, OUT arg2 int)
  BEGIN
CREATE TEMPORARY TABLE ejercicio3(
  numero1 int,
  numero2 int,
  numero3 int);
  INSERT INTO ejercicio3 VALUES
    (num1, num2, num3);
  SELECT GREATEST(numero1, numero2, numero3) mayor, LEAST(numero1,numero2,numero3) menor FROM ejercicio3;

  END//
DELIMITER ;

CALL ej3v1(6,2,5,@4,@7);

/* EJERCICIO 4. Realizar un procedimiento almacenado que muestre cuantos numero1 y numeros 2 son mayores que 50 */
  DROP PROCEDURE IF EXISTS ej4v1;
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej4v1()
  BEGIN
SELECT COUNT(numero1)n1 FROM datos_m3u3ej2 WHERE numero1>50 
  UNION  
 SELECT COUNT(numero2)n2 FROM datos_m3u3ej2 WHERE numero2 >50;
END //
DELIMITER ;
CALL ej4v1();

SELECT * FROM datos_m3u3ej2;
/* EJERCICIO 5. Realizar un procedimiento almacenado que calcule la suma y la resta del numero1 y numero2*/

  DROP PROCEDURE IF EXISTS ej5v1;
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej5v1()
  BEGIN
UPDATE datos_m3u3ej2 JOIN (SELECT id, numero1+numero2 n1 FROM datos_m3u3ej2) c1 USING(id)
  SET suma=n1;
  UPDATE datos_m3u3ej2 JOIN (SELECT id, numero1-numero2 n2 FROM  datos_m3u3ej2) c2 USING (ID)
    SET resta=n2;  
  END //
DELIMITER ;
CALL ej5v1();
SELECT * FROM  datos_m3u3ej2;

/* EJERCICIO 6. Realizar un procedimineto almacenado que primero ponga todos los valores de suma y resta a null y despues 
  la suma solamente si el numero 1 es mayor que el numero 2 y calcule la resta de numero2 menos numero 1
  si el numero 2 es mayor o numero 1 y numero 2 si es mayor que el numero 1*/
    DROP PROCEDURE IF EXISTS ej6v1;
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej6v1()
  BEGIN
  UPDATE datos_m3u3ej2 SET suma=NULL, resta=NULL;
  UPDATE datos_m3u3ej2 SET suma=IF(numero1>numero2,numero1+numero2,NULL);
  UPDATE datos_m3u3ej2 SET resta=IF(numero1>numero2,numero1-numero2,numero2-numero1);
  END //
DELIMITER ;
CALL ej6v1();
SELECT * FROM  datos_m3u3ej2;

/* EJERCICIO 7. Realizar un procedimiento almacenado que coloque en el campo junto el texto1,texto2 */
DROP PROCEDURE IF EXISTS ej7v1;
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7v1 ()
  BEGIN
  UPDATE datos_m3u3ej2 SET junto=CONCAT(texto1,' ',texto2);
  END //
DELIMITER;
CALL ej7v1();
SELECT * FROM datos_m3u3ej2;


/* EJERCICIO 8. Realizar un procedimiento almacenado que coloque en el campo junto el valor NULL.
  Después debe colocar en el campo junto el texto1-texto2 si el rango es A y si es rango B debe colocar texto1+texto.
  Si el rango es distinto debe colocar texto1 nada más  */
  DROP PROCEDURE IF EXISTS ej8v1;
  DELIMITER //
  CREATE OR REPLACE PROCEDURE ej8v1 ()
    BEGIN
      UPDATE datos_m3u3ej2 SET junto=NULL;
      UPDATE datos_m3u3ej2 SET junto=
        IF (rango='A', CONCAT(texto1,'-',texto2), IF(rango='B', CONCAT(texto1,'+',texto2), texto1));
    END //
  DELIMITER;
CALL ej8v1();
SELECT * FROM datos_m3u3ej2;