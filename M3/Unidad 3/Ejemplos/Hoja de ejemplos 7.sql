﻿ALTER TABLE clientes ADD COLUMN edad int;
SELECT * FROM clientes;

-- Disparador 1. Si la edad no está entre 18 y 70, error de edad no válida.

DELIMITER //
CREATE OR REPLACE TRIGGER t1
  BEFORE INSERT
  ON clientes
  FOR EACH ROW
  BEGIN
DECLARE v1 int;       
    
  IF (NEW.edad NOT BETWEEN 18 AND 70) THEN
    CALL p1();
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';
      
  END IF;
  END //
DELIMITER ;

INSERT INTO clientes VALUES (27, 'Federico', 'C/ Palomo', '39300', '05', 75);
SELECT * FROM errores;
SELECT * FROM clientes;

  DELIMITER //
CREATE PROCEDURE p1()
BEGIN
  INSERT INTO errores VALUES (DEFAULT, 'La edad no es valida', NOW(), now());  
END //


-- Disparador 3. Crear un disparador para que cuando modifique un registro de la tabli clientes si no está entre 18 y 70 'La edad no es válida'.

DELIMITER //
CREATE OR REPLACE TRIGGER t1
  BEFORE update
  ON clientes
  FOR EACH ROW
  BEGIN     
    
  IF (NEW.edad NOT BETWEEN 18 AND 70) THEN
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';    
  END IF;

  END //
DELIMITER ;
  
-- Disparador en tabla localidades que compruebe cuando introduzcas modifiques una localidad el código de la provincia exista. En caso de que no exista que la cree.

DELIMITER //
CREATE OR REPLACE TRIGGER t4u
  BEFORE update
  ON localidades
  FOR EACH ROW
  BEGIN     
    
  IF
  NEW.CodPro NOT IN (SELECT CodPro FROM provincias) THEN
    INSERT INTO provincias VALUES (NEW.CodPro, DEFAULT);   
  END IF;

  END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE TRIGGER t4i
  BEFORE insert
  ON localidades
  FOR EACH ROW
  BEGIN     
    
  IF
  NEW.CodPro NOT IN (SELECT CodPro FROM provincias) THEN
    INSERT INTO provincias VALUES (NEW.CodPro, DEFAULT);   
  END IF;

  END //
DELIMITER ;

  INSERT INTO localidades (CodLoc, NombLoc, CodPro)
  VALUES ('10001', 'Potes', '1');

  /* Modificar los disparadores de la tabla clientes para que cuando introduzcas o mofiques un cliente compruebe si la localidad de cliente exista.
    En caso de que no exista mostrar el mensaje de error "La localidad no existe". Debe insertar ese registro de error en la tabla errores. */

DELIMITER //
CREATE OR REPLACE TRIGGER t5i
  BEFORE insert
  ON clientes
  FOR EACH ROW
  BEGIN     
    
  DECLARE numero int DEFAULT 0;
  IF
     (NEW.edad NOT BETWEEN 18 AND 70) THEN
    INSERT INTO errores VALUES (DEFAULT, 'La edad no es valida', NOW(), now());  
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';
  END IF;

  -- Comprobar si la localidad existe.
  SELECT COUNT(*) INTO numero /* AQUI ES COMO HACER EL SET, METES EL COUNT DENTRO DE LA VARIABLE NUMERO */ FROM localidades l
  WHERE l.CodLoc=New.CodLoc;
    IF (numero=0) THEN 
      INSERT INTO errores VALUES (DEFAULT, 'La localidad no existe', NOW(), now());  
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT ='La localidad no existe';
    END IF;

  END //
DELIMITER ;



DELIMITER //
CREATE OR REPLACE TRIGGER t5u
  BEFORE update
  ON clientes
  FOR EACH ROW
  BEGIN     
    
  DECLARE numero int DEFAULT 0;
  IF
     (NEW.edad NOT BETWEEN 18 AND 70) THEN
    INSERT INTO errores VALUES (DEFAULT, 'La edad no es valida', NOW(), now());  
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'la edad no es valida';
  END IF;

  -- Comprobar si la localidad existe.
  SELECT COUNT(*) INTO numero /* AQUI ES COMO HACER EL SET, METES EL COUNT DENTRO DE LA VARIABLE NUMERO */ FROM localidades l
  WHERE l.CodLoc=New.CodLoc;
    IF (numero=0) THEN 
      INSERT INTO errores VALUES (DEFAULT, 'La localidad no existe', NOW(), now());  
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT ='La localidad no existe';
    END IF;

  END //
DELIMITER ;



/* Crear unos disparadores que cuando modifiques o insertes un resgitro te calcule iniciales y compruebe si la provincia por error ya se ha utilizado en localidades. */
ALTER TABLE provincias ADD COLUMN iniciales char(1);
ALTER TABLE provincias ADD COLUMN cantidad int;

  DELIMITER //
CREATE OR REPLACE TRIGGER t6
  BEFORE INSERT
  ON provincias
  FOR EACH ROW
  BEGIN
  DECLARE numero int DEFAULT 0;
  -- obtener las iniciales.
    set new.iniciales=LEFT(new.nombpro,1);

  SELECT COUNT(*) INTO numero FROM localidades l
  WHERE l.CodPro=New.CodPro;

  SET new.cantidad=numero;

  END //
DELIMITER ;



DELIMITER //
CREATE OR REPLACE TRIGGER t7

  AFTER INSERT
  ON localidades
  FOR EACH ROW
  BEGIN
  DECLARE numero int;

  -- Numero de localidades de la pronvicia a la que pertenece la localidad nueva

  SELECT COUNT(*) INTO numero FROM localidades
  WHERE CodPro=new.CodPro;

  UPDATE provincias p
  set p.cantidad = numero
  WHERE CodPro=New.codPro;
  END //
DELIMITER ;
