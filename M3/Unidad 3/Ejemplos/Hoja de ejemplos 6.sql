﻿USE ejemplo6;

SELECT * FROM ventas;


-- Disparador 1. Calcular total automáticamente.
DELIMITER //
CREATE OR REPLACE TRIGGER trigger1
  BEFORE INSERT
  ON ventas
  FOR EACH ROW
  BEGIN
    set NEW.total=(new.precio*new.unidades);

  END //
DELIMITER ;

-- Disparador 2. Al insertar registro suma total a la tabla productos.
SELECT * FROM productos;
SELECT * FROM ventas;
DELIMITER //
CREATE OR REPLACE TRIGGER trigger2
  after INSERT
  ON ventas
  FOR EACH ROW
  BEGIN
    UPDATE productos
    SET cantidad=cantidad+new.total
  WHERE NEW.producto=producto;
  END //
DELIMITER ;

SELECT * FROM productos;
INSERT INTO ventas (id, producto, precio, unidades)
  VALUES (0, 'p2', 15, 6);

SELECT * FROM ventas;


-- Disparador 3. Calcular total automáticamente al actualizar.
DELIMITER //
CREATE OR REPLACE TRIGGER trigger1
  BEFORE update
  ON ventas
  FOR EACH ROW
  BEGIN
    set NEW.total=(new.precio*new.unidades);
  END //
DELIMITER ;

-- Disparador 4. Crear un disparador para la tabla ventas para que cuando actualice un registro me sume el total a la tabla productos (en el campo cantidad).
DELIMITER //
CREATE OR REPLACE TRIGGER trigger4
  after update
  ON ventas
  FOR EACH ROW
  BEGIN
    UPDATE productos
    SET cantidad=cantidad-old.total+new.total
  WHERE NEW.producto=producto;
  END //
DELIMITER ;


-- Disparador 5. Crear un disparador para la tabla productos que si cambia el código del producto te sume todos los totales de ese producto de la tabla ventas.
DELIMITER //
CREATE OR REPLACE TRIGGER trigger5
  BEFORE UPDATE
  ON productos
  FOR EACH ROW
  BEGIN
   set NEW.cantidad=(SELECT SUM(total) FROM ventas
  WHERE producto=new.producto);
  END //
DELIMITER ;

-- Disparador 6. Crear un disparador para la tabla productos que si eliminas un producto te elimine todos los productos del mismo código en la tabla ventas.
DELIMITER //
CREATE OR REPLACE TRIGGER trigger6
  after DELETE
  ON productos
  FOR EACH ROW
  BEGIN
   DELETE FROM ventas WHERE producto=old.producto;
  END //
DELIMITER ;

-- Disparador 7. Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del campo cantidad de la tabla produtos (en el campo cantidad).
  DELIMITER //
CREATE OR REPLACE TRIGGER trigger7
  after DELETE
  ON ventas
  FOR EACH ROW
  BEGIN
    UPDATE productos
      set cantidad=cantidad-old.total;
  END //
DELIMITER ;