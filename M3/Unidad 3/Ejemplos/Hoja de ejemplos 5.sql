﻿
/* Ejercicio 1 Escribe un procedimiento que reciba el nombre de un país como parámetro de entrada y realice una consulta sobre la tabla cliente para obtener todos los clientes
  que existen en la tabla de ese país */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej1(IN argpais varchar(20))
  BEGIN
SELECT * FROM cliente
  WHERE pais=argpais;
  END //
DELIMITER ;

CALL ej1('Spain');

/* Ejercicio 2. Realizar una función que me devuelva el número de registros de la tabla clientes que pertenezcan a un país que le pase por teclado */
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej2(IN argpais varchar(20))
  BEGIN
SELECT COUNT(*) FROM cliente
  WHERE pais=argpais;
  END //
DELIMITER ;

CALL ej2('Spain');

/* Ejercicio 3. Realizar el mismo ejercicio anterior, pero para desplazarme por la tabla utilizar cursores con excepciones. */
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej3(argpais varchar(20))
  BEGIN 
  
  DECLARE final int DEFAULT 0;    -- Esta variable me ayuda para saber si estoy al final de la tabla cuando leo con el cursor
  

  DECLARE valorLeido varchar(20);     -- Variable para utilizar con el cursor


  DECLARE contador int DEFAULT 0;         -- Declaro un contador


  DECLARE c CURSOR FOR        -- Declaro el cursor
    SELECT pais FROM cliente;

  DECLARE CONTINUE HANDLER FOR NOT FOUND        -- Control de excepciones
    set final=1;

  OPEN c;                           -- Inicializamos el cursor
  FETCH c INTO valorLeido;               -- Leo el primer registro
  WHILE (final=0) DO              -- Empiezo el bucle
    IF (valorLeido=argpais) THEN 
      set contador=contador+1;
      
    END IF;
    FETCH c INTO valorLeido;            -- Dentro del bucle, apunto al siguiente registro.
  END WHILE; 
  CLOSE c;
    SELECT contador;
  END //
DELIMITER ;
CALL ej3('Spain');
SELECT * FROM cliente WHERE pais='spain';


/* Ejercicio 4. Escribe una función que le pasas una cantidad y que te devuelva el número total de productos que hay en en la tabla productos cuya cantidad en stock es
  superior a la pasada. */

DELIMITER //
CREATE OR REPLACE FUNCTION ej4(cantidadstock int)
RETURNS int
BEGIN
RETURN (SELECT COUNT(*) FROM producto
  WHERE cantidad_en_stock>=cantidadstock);
END //
DELIMITER ;

SELECT ej4(200);

/* Ejercicio 5. Realizar el mismo ejercicio con cursores */
  DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(cantidadenstock int)
  BEGIN 
  
  DECLARE final int DEFAULT 0;    -- Esta variable me ayuda para saber si estoy al final de la tabla cuando leo con el cursor
  

  DECLARE valorLeido int;     -- Variable para utilizar con el cursor


  DECLARE contador int DEFAULT 0;         -- Declaro un contador


  DECLARE c CURSOR FOR        -- Declaro el cursor
    SELECT cantidad_en_stock FROM producto;

  DECLARE CONTINUE HANDLER FOR NOT FOUND        -- Control de excepciones
    set final=1;

  OPEN c;                           -- Inicializamos el cursor
  FETCH c INTO valorLeido;               -- Leo el primer registro
  WHILE (final=0) DO              -- Empiezo el bucle
    IF (valorLeido>=cantidadenstock) THEN 
      set contador=contador+1;
      
    END IF;
    FETCH c INTO valorLeido;            -- Dentro del bucle, apunto al siguiente registro.
  END WHILE; 
  CLOSE c;
    SELECT contador;
  END //
DELIMITER ;
CALL ej5(200);

/* Ejercicio 6. Escribe un procedimiento que reciba como parámetros de entrada una forma de pago, que será una cadena de caracteres
  (Ejemplo: Paypal, Transferencia, etc). Y devuelva como salida el pago de máximo valor realizado para esa forma de pago. Deberá hacer uso de la tabla
  pago de la base de datos jardinería2. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej6 (modopago varchar(20))
  BEGIN
SELECT MAX(total) FROM pago
  WHERE forma_pago=modopago;
  END //
DELIMITER ;

CALL ej6('PayPal');

/* Ejercicio 7. Escribe un procedimietno que reciba como parámetro de entrada una forma de pago, que será una cadena de caracteres
  (Ejemplo: Paypal, Transferencia, etc) Y devuelva como salida los siguientes valores teniendo en cuenta la forma de pago seleccionada
  como parámetro de entrada: 
  - el pago de máximo valor,
  - el pago de mínimo valor,
  - el valor medio de los pagos realizados,
  - la suma de todos los pagos,
  - el número de pagos realizados para esa forma de pago. */

  DELIMITER //
CREATE OR REPLACE PROCEDURE ej7 (modopago varchar(20))
  BEGIN
SELECT MAX(total) MaximoValor, MIN(total) MinimoValor, AVG(total) Media, SUM(total) Suma, COUNT(*) NumerodeValores FROM pago
  WHERE forma_pago=modopago;
  END //
DELIMITER ;

CALL ej7('Paypal');

/* Ejercicio 8. Escribe una función para la base de datos tienda que devuelva el valor medio del precio de los productos de un determinado
  fabricante que se recibirá como parámetro de entrada. El parámetro de entrada será el nombre del fabricante. */

DELIMITER //
CREATE OR REPLACE FUNCTION ej8(prov varchar(20))
  RETURNS float
  BEGIN
  DECLARE media float;
  set media=(SELECT AVG(precio_venta) FROM producto
  WHERE proveedor=prov);
  RETURN media;
  END //
  DELIMITER ;

SELECT * FROM producto;
SELECT ej8('HiperGarden Tools');

/* Ejercicio 9. Realizar el mismo ejercicio anterior, pero haciendo que en caso de que el fabricante pasado no tenga productos produzca una
  excepción personalizada que muestre el error fabricante no existe. */

  DELIMITER //
CREATE OR REPLACE FUNCTION ej8(prov varchar(20))
  RETURNS float
  BEGIN
  DECLARE media float;
  
  IF ((SELECT count(*) FROM producto
    WHERE proveedor=prov)=0) THEN 
    SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'El proveedor no existe';
  END IF;
  set media=(SELECT AVG(precio_venta) FROM producto
  WHERE proveedor=prov);
  
  RETURN media;
  END //
  DELIMITER ;

SELECT ej8('i');