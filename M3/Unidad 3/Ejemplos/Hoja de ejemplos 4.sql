﻿-- Ejercicio 1. Crea un procedimiento simple que muestre por pantalla el texto "esto es una ejemplo de procedimiento" (donde la cabecera de la columna sea "mensaje")

DELIMITER //
  CREATE OR REPLACE PROCEDURE ej1()
    BEGIN
  SELECT 'Esto es un ejemplo de procedimiento' mensaje;
    END //
  DELIMITER ;

CALL ej1();

/* Ejercicio 2. Crea un procedimiento donde se declaren tres variables internas, una de tipo entero con valor de 1, otra de tipo
  varchar(10) con valor nulo por defecto y otra de tipo decimal con 4 dígitos y 2 decimales con valor 10,48 por defecto. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej2()
  BEGIN
  DECLARE var1 int DEFAULT 1;
  DECLARE var2 varchar(10) DEFAULT NULL;
  DECLARE var3 decimal(4,2) DEFAULT 10.48;
  END //
DELIMITER ;

/* Ejercicio 3. Copiar, aumentar y corregir los errores en el siguiente código. */

/* Ejercicio 4. Escribe un procedimiento que reciba un parámetro de entrada (numero real) y muestre el numero en pantalla. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej4(valor int)
  BEGIN
  SELECT valor;
  END //
DELIMITER ;

CALL ej4(7);

/* Ejercicio 5.
Escribe un procedimiento que reciba un parámetro de entrada y asigne ese parámetro a una variable interna del mismo tipo. Después mostrar la variable en pantalla. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej5(IN variable int)
  BEGIN
  DECLARE var int;
  SET var=variable;
  SELECT var;
  END //
DELIMITER ;

CALL ej5(3);

/* Ejercicio 6. 
Escribe un procedimiento que reciba un número real de entrada y muestre un mensaje indicando si el número es positivo, negativo o 0. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej6(IN variable int)
  BEGIN
  DECLARE valor varchar(15);
  SET valor= IF(variable<0,'Negativo',IF(variable=0,'Cero','Positivo'));
  SELECT valor;
  END //
DELIMITER ;

CALL ej6(-5);

/* Ejercicio 7. 
Escribe una función que reciba un número de entrada y devuelva TRUE si el número es par o FALSE en caso contrario. */
DELIMITER //
CREATE OR REPLACE PROCEDURE ej7(IN variable int)
  BEGIN
  DECLARE valor varchar(15);
  set valor= IF(MOD(variable,2)=0, 'TRUE', 'FALSE');
  SELECT valor;
  END //
DELIMITER ;

CALL ej7(6);

/* Ejercicio 8.
  Escribe una función que devuelva el valor de la hipotenusa de un triángulo a partir de los valores de sus lados */
DELIMITER //
CREATE OR REPLACE FUNCTION Pitagoras(lado1 float, lado2 float)
  RETURNS int
  BEGIN
  DECLARE hipotenusa int;
  set hipotenusa=(POW(lado1,2)+POW(lado2,2));
  RETURN hipotenusa;
  END //
DELIMITER ;

SELECT Pitagoras(5,7);


/* Ejercicio 9. Modifique el procedimiento diseñado en el ejercicio anterior para que tenga un parámetro de entrada con el valor de un número real
                y un parámetro de salida, con una cadena de caracteres indicando si el número es positivo, negativo o cero. */

  DELIMITER //
CREATE OR REPLACE PROCEDURE ej9(IN variable int, OUT salida varchar(15))
  BEGIN
  SET salida= IF(variable<0,'Negativo',IF(variable=0,'Cero','Positivo'));
    END //
DELIMITER ;

CALL ej9(5,@s);
SELECT @s;

/* Ejercicio 10. Escribe un procedimiento que reciba un número real de entrada, que representa el valor de la nota de un alumno, y muestre
                  un mensaje indicando qué nota ha obtenido teniendo en cuenta las siguientes condiciones. */

DELIMITER //
CREATE OR REPLACE PROCEDURE EJ10(nota int)
  BEGIN

  DECLARE notaleida varchar(15);
    CASE
     WHEN (nota<5) THEN SET notaleida= 'Suspenso' ;
     WHEN (nota<6) THEN set notaleida= 'Suficiente';
     WHEN (nota<7) THEN set notaleida= 'Bien';
     WHEN (nota<9) THEN SET notaleida='Notable';
     WHEN (nota<=10) THEN set notaleida= 'Sobresaliente';
     ELSE SET notaleida='No valido';
     END CASE;
  SELECT notaleida;
  END //
  DELIMITER ;

CALL EJ10(9);

/* Ejercicio 11. Escribe un procedimiento que me cree una tabla llamada notas en caso de que no exista. El procedimiento recibirá dos argumentos de entrada que 
                  son nombre y nota (numero real). */

  DELIMITER //
CREATE OR REPLACE PROCEDURE ej11(nombre varchar(15), nota int)
  BEGIN
  CREATE OR REPLACE TEMPORARY TABLE notas(
  alumno varchar(15),
  nota int,
  PRIMARY KEY (alumno)
  );
  INSERT INTO notas VALUES (nombre, nota);
  END //
DELIMITER ;

CALL ej11 ('Pablo', 7);

SELECT * FROM notas;

/* Ejercicio 12. Escribe una función que reciba como parámetro de entrada un valor numérico que represente un día de la semana y que devuelva
                 una cadena de caracteres con el nombre del día de la semana correspondiente. Por ejemplo, para el valor de entrada 1 debería
                 devolver la cadena lunes. */
DELIMITER //
CREATE OR REPLACE FUNCTION ej12(dia int)
  RETURNS varchar(15)
  BEGIN
  DECLARE dialetra varchar(15);
  set dialetra=IF(dia=1, 'lunes', IF(dia=2, 'martes', IF(dia=3, 'miércoles', IF(dia=4, 'jueves', IF(dia=5, 'viernes', IF(dia=6, 'sábado', 'domingo'))))));
  RETURN dialetra;
  END //
DELIMITER ;

SELECT ej12(5);

/* Ejercicio 13. Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla notas con ese nombre. Realizarlo
                sin utilizar cursores. */

DELIMITER //
CREATE OR REPLACE PROCEDURE ej13(nombre varchar(15))
  BEGIN
  SELECT COUNT(*) FROM notas
  WHERE nombre = nombre;
  END //
DELIMITER ;      

CALL ej13('Pablo');  


/* Ejercicio 14. Crear un procedimiento almacenado que le pasemos un nombre de alumno y me devuelva cuantos alumnos hay en la tabla con ese nombre. Realizarlo
                utilizando cursores. */ 