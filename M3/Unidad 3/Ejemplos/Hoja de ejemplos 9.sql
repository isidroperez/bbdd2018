﻿USE ejemplo9;

DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpleados(test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  CREATE OR REPLACE TABLE empleados(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    fechaNacimiento date,
    correo varchar(20),
    edad int,
    PRIMARY KEY (id)
    )ENGINE MYISAM;

  IF(test=1) THEN
    INSERT INTO empleados (nombre, fechaNacimiento)
      VALUES 
      ('Pedro','2000/1/1'),
      ('Ana','1980/10/1'),
      ('Jose','1990/12/4'),
      ('Luis','1985/4/9'),
      ('Ernesto','1975/4/4');
  END IF;

END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablaEmpresa (test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  -- crear la tabla
  CREATE OR REPLACE TABLE empresa(
    id int AUTO_INCREMENT,
    nombre varchar(20),
    direccion varchar(20),
    numeroEmpleados int,
    PRIMARY KEY (id)
    )ENGINE MYISAM;
  -- introducir datos de test
  IF(test=1) THEN
    INSERT INTO empresa (nombre, direccion) VALUES 
      ('empresa1','direccion1'),
      ('empresa2','direccion2'),
      ('empresa3','direccion3'),
      ('empresa4','direccion4'),
      ('empresa5','direccion5');
  END IF ;
END //
DELIMITER ;


DELIMITER //
CREATE OR REPLACE PROCEDURE crearTablatrabajan(test int)
BEGIN
/* variables */
/* cursores */
/* excepciones */
/* programa */
  CREATE OR REPLACE TABLE trabajan(
    id int AUTO_INCREMENT,
    empleado int,
    empresa int,
    fechaInicio date,
    fechaFin date,
    baja bool DEFAULT FALSE,
    PRIMARY KEY(id),
    CONSTRAINT trabajanUK UNIQUE KEY (empleado,empresa)
    )ENGINE MYISAM;

  IF (test=1) THEN 
    INSERT INTO trabajan (empleado, empresa, fechaInicio, fechaFin) VALUES 
      (1,1,'2015/1/1',NULL);
  END IF;

END //
DELIMITER ;




DELIMITER //
CREATE OR REPLACE TRIGGER trabajanBI
  BEFORE INSERT
  ON trabajan
  FOR EACH ROW
  BEGIN
  -- Si metes una fecha de inicio mayor a la de fin error de fecha de inicio no válida.
    IF(new.fechaInicio>new.fechaFin)
      THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

  -- Si la fecha de fin no está vacía la baja se pondrá a true, si no a false.
    IF (new.FechaFin) IS NOT NULL THEN
      SET new.baja=TRUE;
    END IF;

  -- Si el numero de trabajador no existe en la tabla trabajadores no te deja meter el dato

    IF (existetrabajador(new.empleado)=0) THEN
      SIGNAL SQLSTATE '45000' set MESSAGE_TEXT = 'El trabajador no existe';
    END IF;

  -- Si el numero de empresa no existe en la tabla empresa no te deja meter el dato

    IF (existeempresa(new.empresa)=0) THEN
      SIGNAL SQLSTATE '45000' set MESSAGE_TEXT = 'La empresa no existe';
    END IF;
  END //
DELIMITER;



DELIMITER //
CREATE OR REPLACE TRIGGER trabajanBU
  BEFORE update
  ON trabajan
  FOR EACH ROW
  BEGIN
  -- Si metes una fecha de inicio mayor a la de fin error de fecha de inicio no válida.
    IF(new.fechaInicio>new.fechaFin)
      THEN
      SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'La fecha de inicio no es válida';
    END IF;

  -- Si la fecha de fin no está vacía la baja se pondrá a true, si no a false.
    IF (new.FechaFin) IS NOT NULL THEN
      SET new.baja=TRUE;
    ELSE
      set new.baja=FALSE;
    END IF;

  -- Si el numero de trabajador no existe en la tabla trabajadores no te deja meter el dato

    IF (existetrabajador(new.empleado)=0) THEN
      SIGNAL SQLSTATE '45000' set MESSAGE_TEXT = 'El trabajador no existe';
    END IF;

  -- Si el numero de empresa no existe en la tabla empresa no te deja meter el dato

    IF (existeempresa(new.empresa)=0) THEN
      SIGNAL SQLSTATE '45000' set MESSAGE_TEXT = 'La empresa no existe';
    END IF;

  END //
DELIMITER;


/* Crear una función que reciba como argumento un id de empresa y me devuelva si la empresa existe. */
DELIMITER //
CREATE OR REPLACE FUNCTION existeempresa(empresa int)
  RETURNS int
  BEGIN
  DECLARE numero int;
  SELECT COUNT(*) INTO numero FROM empresa
  WHERE id=empresa;

  RETURN numero;
  END //
DELIMITER ;



/* Crear una función que reciba como argumento un id de empleado y me devuelva si el empleado existe. */

DELIMITER //
CREATE OR REPLACE FUNCTION existetrabajador(emple int)
RETURNS int
  BEGIN
  DECLARE numero int;
  SELECT COUNT(*) INTO numero FROM empleados
  WHERE id=emple;

  RETURN numero;
END //
DELIMITER ;




DELIMITER //
CREATE OR REPLACE TRIGGER empleadosBI
  BEFORE INSERT
  ON empleados
  FOR EACH ROW
  BEGIN
  SET new.edad=TIMESTAMPDIFF(year,new.fechaNacimiento,NOW());
  END //
DELIMITER ;
INSERT INTO empleados (id, nombre, fechaNacimiento, correo)
  VALUES (default, 'Paco', '1996-09-05', '');
SELECT * FROM empleados;


DELIMITER //
CREATE OR REPLACE TRIGGER empleadosBU
  BEFORE UPDATE
  ON empleados
  FOR EACH ROW
  BEGIN
  SET new.edad=TIMESTAMPDIFF(year,new.fechaNacimiento,NOW());
  END //
DELIMITER ;